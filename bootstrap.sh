#!/usr/bin/env bash

# Update package list and upgrade all packages
apt-get update
apt-get -y upgrade

# Begin PostgreSQL installation

# Edit the following to change the name of the database user that will be created:
APP_DB_USER=freischeisse
APP_DB_PASS=BEDmg4RTwqzcyfouu3X8

# Edit the following to change the name of the database that is created (defaults to the user name)
APP_DB_NAME=freischeisse

# Edit the following to change the version of PostgreSQL that is installed
PG_VERSION=9.3

export DEBIAN_FRONTEND=noninteractive

DB_PROVISIONED=/etc/db_provisioned
if [ ! -f "$DB_PROVISIONED" ]
  then
  PG_REPO_APT_SOURCE=/etc/apt/sources.list.d/pgdg.list
  if [ ! -f "$PG_REPO_APT_SOURCE" ]
  then
    # Add PG apt repo:
    echo "deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main" > "$PG_REPO_APT_SOURCE"

    # Add PGDG repo key:
    wget --quiet -O - http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | apt-key add -
  fi

  apt-get update
  apt-get -y upgrade
  apt-get install -y "postgresql-$PG_VERSION" "postgresql-contrib-$PG_VERSION" "postgresql-server-dev-$PG_VERSION"

  PG_CONF="/etc/postgresql/$PG_VERSION/main/postgresql.conf"
  PG_HBA="/etc/postgresql/$PG_VERSION/main/pg_hba.conf"
  PG_DIR="/var/lib/postgresql/$PG_VERSION/main"

  # Edit postgresql.conf to change listen address to '*':
  sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" "$PG_CONF"

  # Append to pg_hba.conf to add password auth:
  echo "host    all             all             all                     md5" >> "$PG_HBA"

  # Explicitly set default client_encoding
  echo "client_encoding = utf8" >> "$PG_CONF"

  # Restart so that all new config is loaded:
  service postgresql restart

  cat << EOF | su - postgres -c psql
  -- Create the database user:
  CREATE USER $APP_DB_USER WITH PASSWORD '$APP_DB_PASS';

  -- Create the database:
  CREATE DATABASE $APP_DB_NAME WITH OWNER=$APP_DB_USER
                                    LC_COLLATE='en_US.utf8'
                                    LC_CTYPE='en_US.utf8'
                                    ENCODING='UTF8'
                                    TEMPLATE=template0;
EOF

su - postgres -c "psql freischeisse -c 'CREATE EXTENSION hstore;'"

# Tag the provision time:
date > "$DB_PROVISIONED"
fi

# End PostgreSQL installation


# Install PIP
apt-get install -y python3-pip

pip3 install --upgrade pip

# Install the python requirements
/usr/local/bin/pip3 install -r /vagrant/requirements.txt

# locale
echo "pt_BR.UTF-8 UTF-8" | sudo tee -a /var/lib/locales/supported.d/local
sudo dpkg-reconfigure locales

