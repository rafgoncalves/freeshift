from app.models import WorkEntry, Company, User, Contract, Assignment, Project
from behave import then
from behave import when
from behave import given
import json
import requests
from config import SECRET_KEY
import jwt
from datetime import datetime, timedelta
from peewee import JOIN
from config import DATABASE

db = DATABASE
#############################################################
# def before_all(context):
# context.server = simple_server.WSGIServer(('', 8000))
# context.server.set_app(web_app.main(environment='test'))
# context.thread = threading.Thread(target=context.server.serve_forever)
#     context.thread.start()
#     context.browser = webdriver.Chrome()
#############################################################

###################          MESSAGES          ##############################

@then('message 200')
def step_impl(context):
    assert context.result.status_code == 200


@then('message 201')
def step_impl(context):
    assert context.result.status_code == 201


@then('message 204')
def step_impl(context):
    assert context.result.status_code == 204


@then('message 202')
def step_impl(context):
    assert context.result.status_code == 202


@then('message 400')
def step_impl(context):
    assert context.result.status_code == 400


@then('message 401')
def step_impl(context):
    assert context.result.status_code == 401

@then('message 404')
def step_impl(context):
    assert context.result.status_code == 404

#################         END MESSAGES       #################################


####################          LOGIN        ##################################

# @given('worker is logged in')
# def step_impl(context):
#     context.user_id = 2
#     context.company_id = 1
#     context.name = 'Bob'
#     context.data_local = {'username': context.name.lower(), 'password': context.name}
#     context.auth = requests.post("http://127.0.0.1:5000/services/login/", json=context.data_local)
#     context.json_data = context.auth.json()
#     context.payload = jwt.decode(context.json_data['token'].encode(encoding='ASCII'), key=SECRET_KEY)
#     # print(context.json_data['token'])
#     # print("payload: "+str(context.payload))
#
# @given('admin is logged in')
# def step_impl(context):
#     context.user_id = 1
#     context.company_id = 1
#     context.name = 'Alice'
#     context.data_local = {'username': context.name.lower(), 'password': context.name}
#     context.auth = requests.post("http://127.0.0.1:5000/services/login/", json=context.data_local)
#     context.json_data = context.auth.json()
#     context.payload = jwt.decode(context.json_data['token'].encode(encoding='ASCII'), key=SECRET_KEY)
#
# @given('coordinator is logged in')
# def step_impl(context):
#     context.user_id = 6
#     context.company_id = 3
#     context.name = 'Gene'
#     context.data_local = {'username': context.name.lower(), 'password': context.name}
#     context.auth = requests.post("http://127.0.0.1:5000/services/login/", json=context.data_local)
#     context.json_data = context.auth.json()
#     context.payload = jwt.decode(context.json_data['token'].encode(encoding='ASCII'), key=SECRET_KEY)

###################       END LOGIN        ####################################


###################       START CASO 1      #######################

@given('user provides login and password ok as worker')
def step_impl(context):
    context.login = 'bob'
    context.password = 'Bob'


@given('user provides login but password')
def step_impl(context):
    context.login = 'bob'
    context.password = ''


@given('user provides a wrong password')
def step_impl(context):
    context.login = 'bob'
    context.password = 'Bobb'


@given('bad request for authentication')
def step_impl(context):
    context.login = 'bob'
    context.password = None


@when('user sends an authentication request')
def step_impl(context):
    data_local = {'username': context.login, 'password': context.password}
    context.result = requests.post("http://127.0.0.1:5000/services/login/", json=data_local)


@then('json has token')
def step_impl(context):
    assert 'token' in context.result.json()

################        END CASO 1      #############################


###############       START CASO 2       ###############

@given('user provides login and password ok as admin')
def step_impl(context):
    context.login = 'alice'
    context.password = 'Alice'

@given('user provides login and password ok as coordinator')
def step_impl(context):
    context.login = 'gene'
    context.password = 'Gene'

@then('user role is admin')
def step_impl(context):
    json_data = context.result.json()
    payload = jwt.decode(json_data['token'].encode(encoding='ASCII'), key=SECRET_KEY)
    for company in payload['companies']:
        if company['role'] == 'ADMIN':
            assert True
            break

@then('user role is worker')
def step_impl(context):
    json_data = context.result.json()
    payload = jwt.decode(json_data.get('token').encode(encoding='ASCII'), key=SECRET_KEY)
    for company in payload['companies']:
        if company['role'] == 'WORKER':
            assert True
            break

@then('user role is coordinator')
def step_impl(context):
    json_data = context.result.json()
    payload = jwt.decode(json_data.get('token').encode(encoding='ASCII'), key=SECRET_KEY)
    for company in payload['companies']:
        if company['role'] == 'COORDINATOR':
            assert True
            break

#############         END CASO 2       #######################


###############       NOTIFICAÇÃO        ################
###############       START CASO 3       ################

@given('has notification')
def step_impl(context):
    assert True is not False


@given('does not have notification')
def step_impl(context):
    assert True is not False


@then('worker gets notification')
def step_impl(context):
    assert True is not False


@then('worker does not get notification')
def step_impl(context):
    assert True is not False

##############       END CASO 3       ################


#############       START CASO 4       ################

@given('worker gets notification')
def step_impl(context):
    assert True is not False


@when('worker accept project')
def step_impl(context):
    assert True is not False


@when('worker does not accept project')
def step_impl(context):
    assert True is not False

@then('notification disappear')
def step_impl(context):
    assert True is not False

#############         END CASO 4           #######################
#############        END NOTIFICAÇÃO       #######################


###############        WORK ENTRIES       ####################
###############        START CASO 5       ####################

@given('entries context')
def step_impl(context):
    db.connect()
    context.query_entry_user = (WorkEntry.select(WorkEntry.id,
                                                 WorkEntry.created_at,
                                                 WorkEntry.start,
                                                 WorkEntry.finish,
                                                 WorkEntry.status,
                                                 WorkEntry.assignment,
                                                 WorkEntry.obs)
                                .join(Assignment, JOIN.LEFT_OUTER)
                                .join(Project, JOIN.LEFT_OUTER)
                                .join(Contract, JOIN.LEFT_OUTER, on=Assignment.contract == Contract.id)
                                .join(User, JOIN.LEFT_OUTER)).where(Contract.user == context.user_id)

    try:
        query = context.query_entry_user.select(WorkEntry.finish).order_by(WorkEntry.finish.desc()).get()
    except WorkEntry.DoesNotExist:
        query = None
    if query is None:
        WorkEntry.create(start='2012-05-23',
                         finish='2012-05-24',
                         assignment=context.payload['assignments'][0]['assignment_id'],
                         obs='inserindo entries para finalidade de teste, pois não havia nenhuma',
                         created_at=datetime.now(),
                         status='WAITING_APPROVAL')
        WorkEntry.create(start='2008-05-23',
                         finish='2008-05-24',
                         assignment=context.payload['assignments'][0]['assignment_id'],
                         obs='inserindo entries para finalidade de teste, pois não havia nenhuma',
                         created_at=datetime.now(),
                         status='APPROVED')

    query = context.query_entry_user.select(WorkEntry.start).order_by(WorkEntry.start.asc())
    context.entry_min_date = query[0].start

    query = context.query_entry_user.select(WorkEntry.finish).order_by(WorkEntry.finish.desc())
    context.entry_max_date = query[0].finish

    db.close()
    print(str(context.entry_min_date))
    print(str(context.entry_max_date))

@given('date interval ok')
def step_impl(context):
    context.period = dict(start_date=context.entry_min_date, end_date=context.entry_max_date)

@given('bad request to visualize entry log')
def step_impl(context):
    context.period = dict(start_date=None, end_date=context.entry_max_date)

@given('negative date interval')
def step_impl(context):
    context.period = dict(start_date=context.entry_max_date, end_date=context.entry_min_date)

@when('user tries to visualize entry log')
def step_impl(context):
    context.result = requests.get("http://127.0.0.1:5000/api/user/workdays/", params=context.period,
                                  headers={'Authorization': context.auth.json().get('token')})
    print('visualizing entry ' +str(context.name))

@then('get user entry log')
def step_impl(context):
    is_same_username = True
    json_data = context.result.json()
    #print("%s" % json_data)
    for object in json_data['objects']:
        for entry in object['work_entries']:
            if entry['user'] != context.name:
                is_same_username = False
                break
    assert is_same_username

@then('get user empty entry log')
def step_impl(context):
    assert len(context.result.json()['objects']) == 0

#############       END CASO 5       #######################


###############      START CASO 6       ###############

@given('user has assignments')
def step_impl(context):
    #  json_data = context.auth.json()
    # # print(json_data['token'])
    #  payload = jwt.decode(json_data['token'].encode(encoding='ASCII'), key=SECRET_KEY)
    # # print(str(payload))
    #  if len(payload['assignments']) == 0:
    assert len(context.payload['assignments']) > 0

@given('no period conflict')
def step_impl(context):
    start = context.entry_max_date + timedelta(days=1)
    #print(str(start))
    finish = start + timedelta(days=0.5)
    #print(str(finish))

    context.data = {'startTime': start.strftime('%Y-%m-%dT%H:%M:%S'),
                    'endTime': finish.strftime('%Y-%m-%dT%H:%M:%S'),
                    'assignment_id': context.payload['assignments'][0]['assignment_id'],
                    'observation': 'testando inserção/atualização de workentries sem conflito'}

@given('period conflict')
def step_impl(context):
    start = context.entry_min_date
    print('Data mínima para conflito de período'+str(start))
    finish = start + timedelta(days=1)
    #print(str(finish))

    context.data = {'startTime': start.strftime('%Y-%m-%dT%H:%M:%S'),
                    'endTime': finish.strftime('%Y-%m-%dT%H:%M:%S'),
                    'assignment_id': context.payload['assignments'][0]['assignment_id'],
                    'observation': 'testando inserção de workentries com conflito/inconsistência de dados'}


@given('negative date interval with no period conflict')
def step_impl(context):
    start = context.entry_max_date + timedelta(days=1)
    finish = context.entry_max_date + timedelta(days=0.5)
    context.data = {'startTime': start.strftime('%Y-%m-%dT%H:%M:%S'),
                    'endTime': finish.strftime('%Y-%m-%dT%H:%M:%S'),
                    'assignment_id': context.payload['assignments'][0]['assignment_id'],
                    'observation': 'testando inserção de workentries sem conflito e com início maior que fim.'}


@when('adding entry')
def step_impl(context):
    context.model = WorkEntry
    context.url = "http://127.0.0.1:5000/api/workentries/"
    context.result = requests.post(context.url, json=context.data,
                                   headers={'Authorization': context.auth.json().get('token')})
    print(context.result.status_code)
    print('adding entry ' +str(context.name))

@then('log has entry')
def step_impl(context):
    is_same_entry = False
    json_data_post_put = context.result.json()
    #print("POST/PUT: %s" % json_data_post_put)
    if 'id' in json_data_post_put:
        context.id = json_data_post_put['id']
    if json_data_post_put['start'] == context.data['startTime'] and json_data_post_put['finish'] == context.data['endTime']:
        is_same_entry = True
    assert is_same_entry

    context.period = dict(start_date=context.entry_min_date, end_date=context.entry_max_date+timedelta(days=2))
    context.result = requests.get("http://127.0.0.1:5000/api/user/workdays/", params=context.period,
                                  headers={'Authorization': context.auth.json().get('token')})

    entry_added = False
    json_data_get = context.result.json()
    #print("GET: %s" % json_data_get)
    for object in json_data_get['objects']:
        for entry in object['work_entries']:
            if entry['id'] == context.id:
                #print("ID: %d" % context.id)
                entry_added = True
                break
    assert entry_added

#############       END CASO 6       #######################


###############      START CASO 7      ###############

@given('entry waiting approval')
def step_impl(context):
    create_entry_waiting_approval = False

    db.connect()
    query = context.query_entry_user.where(WorkEntry.status == 'WAITING_APPROVAL')
    try:
        entry_waiting_approval = query.get()
    except WorkEntry.DoesNotExist:
        entry_waiting_approval = None

    if entry_waiting_approval is not None:
        if query.where(WorkEntry.start != context.entry_min_date).exists():
            context.id = query.select(WorkEntry.id).where(WorkEntry.start != context.entry_min_date).get().id
        else:
            create_entry_waiting_approval = True
    else:
        create_entry_waiting_approval = True

    if create_entry_waiting_approval:
        WorkEntry.create(start=context.entry_max_date + timedelta(days=1),
                                 finish=context.entry_max_date + timedelta(days=1.25),
                                 assignment=context.payload['assignments'][0]['assignment_id'],
                                 obs='inserindo entry com status WAITING_APPROVAL, pois não tinha nenhuma',
                                 created_at=datetime.now(),
                                 status='WAITING_APPROVAL')
        context.id = query.select(WorkEntry.id).where(WorkEntry.start != context.entry_min_date).get().id

    db.close()
    print(context.id)


@given('entry approved')
def step_impl(context):
    db.connect()
    try:
        context.id = context.query_entry_user.select(WorkEntry.id).where(WorkEntry.status == 'APPROVED').get().id
    except WorkEntry.DoesNotExist:
        context.id = None

    if context.id is None:
        WorkEntry.create(start=context.entry_max_date + timedelta(days=1),
                         finish=context.entry_max_date + timedelta(days=1.5),
                         assignment=context.payload['assignments'][0]['assignment_id'],
                         obs='inserindo entry com status APPROVED, pois não tinha nenhuma',
                         created_at=datetime.now(),
                         status='APPROVED')
        context.id = context.query_entry_user.select(WorkEntry.id).where(WorkEntry.status == 'APPROVED').get().id

    db.close()
    print(context.id)

@when('editing entry')
def step_impl(context):
    context.model = WorkEntry
    context.url = "http://127.0.0.1:5000/api/workentries/"+str(context.id)+"/"
    context.result = requests.put(context.url, json=context.data,
                                  headers={'Authorization': context.auth.json().get('token')})
    print(context.result.status_code)
    print('editing entry ' +str(context.name))
    #print("DATA: %s" % context.data)
    #print("JSON: "+str(context.result.json()))

###############        END CASO 7           #######################


###############        START CASO 8         ###############

@when('deleting entry')
def step_impl(context):
    context.model = WorkEntry
    context.url = "http://127.0.0.1:5000/api/workentries/"+str(context.id)+"/"
    context.result = requests.delete(context.url, headers={'Authorization': context.auth.json().get('token')})
    print(context.result.status_code)
    print('deleting entry ' +str(context.name))
   # print("JSON DELETE: "+str(context.result.json()))

@given('entry does not exist')
def step_impl(context):
    context.id = -1

@then('log does not have entry')
def step_impl(context):
    context.period = dict(start_date=context.entry_min_date, end_date=context.entry_max_date + timedelta(days=2))
    context.result = requests.get("http://127.0.0.1:5000/api/user/workdays/", params=context.period,
                                  headers={'Authorization': context.auth.json().get('token')})
    entry_exist = False
    json_data_get = context.result.json()
    #print("GET: %s" % json_data_get)
    for object in json_data_get['objects']:
        for entry in object['work_entries']:
            if entry['id'] == context.id:
                #print("ID: %d" % context.id)
                entry_exist = True
                break
    assert not entry_exist

###############           END CASO 8              #######################
###############        END WORK ENTRIES        ############################


###############       START CASO 9        ######################

@when('execute all workers tests')
def step_impl(context):
    print('executando worker testes como admin')
    context.execute_steps("""
        Given entries context
        And date interval ok
        When user tries to visualize entry log
        Then message 200
        And get user entry log""")
    context.execute_steps("""
        Given entries context
        And negative date interval
        When user tries to visualize entry log
        Then message 200
        And get user empty entry log""")
    context.execute_steps("""
        Given entries context
        And bad request to visualize entry log
        When user tries to visualize entry log
        Then message 400""")
    context.execute_steps("""
        Given entries context
        And user has assignments
        And no period conflict
        When adding entry
        Then message 201
        And log has entry""")
    context.execute_steps("""
        Given entries context
        And user has assignments
        And period conflict
        When adding entry
        Then message 400""")
    context.execute_steps("""
        Given entries context
        And entry waiting approval
        And no period conflict
        When editing entry
        Then message 202
        And log has entry""")
    context.execute_steps("""
        Given entries context
        And entry waiting approval
        And period conflict
        When editing entry
        Then message 400""")
    # context.execute_steps("""
    #     Given entries context
    #     And entry approved
    #     And no period conflict
    #     When editing entry
    #     Then message 400""")
    context.execute_steps("""
        Given entries context
        And entry waiting approval
        When deleting entry
        Then message 204
        And log does not have entry""")
    context.execute_steps("""
        Given entries context
        And entry does not exist
        When deleting entry
        Then message 404""")
    # context.execute_steps("""
    #     Given entries context
    #     And entry approved
    #     When deleting entry
    #     Then message 400""")

    #print(context.name)
    #print(str(context.entry_min_date))
    #print(str(context.entry_max_date))

    #assert True is not False


@then('no test fails')
def step_impl(context):
    pass

###############        END CASO 9          #######################


###############       START CASO 10        #######################

@when('admin tries to visualize his workers entry log')
def step_impl(context):
    assert True is not False


@then('admin gets his workers entry log and only them')
def step_impl(context):
    assert True is not False


@given('admin can visualize his workers entry log')
def step_impl(context):
    assert True is not False


@when('admin tries to approve or disapprove his workers entry work')
def step_impl(context):
    assert True is not False


@then('update list of pending approval')
def step_impl(context):
    assert True is not False


@then('update entry status')
def step_impl(context):
    assert True is not False

##############        END CASO 10      ###################


################            CRUD COMPANY                  #####################
################        START CASO 11.1 - CREATE          #####################

@when('creating a company')
def step_impl(context):
    context.model = Company
    context.data_local = {'name': 'NEW'}
    context.result = requests.post("http://127.0.0.1:5000/api/companies/",
                                   headers={'Authorization': context.auth.json().get('token')},
                                   json=context.data_local)

@then("update company's list")
def step_impl(context):
    #print("%s" % context.result.json()['name'])
    context.id = context.result.json().get('id')
    assert context.result.json().get('name') == context.data_local['name']

    context.result = requests.get("http://127.0.0.1:5000/api/companies/",
                                headers={'Authorization': context.auth.json().get('token')})
    is_same_company = False
    json_data = context.result.json()
    print("%s" % json_data)
    for object in json_data['objects']:
        if object['id'] == context.id and object['name'] == context.data_local['name']:
            is_same_company = True
            break
    assert is_same_company

#############        END CASO 11.1 - CREATE       #################


#############        START CASO 11.2 - READ       #################


@when("user tries to visualize company's list")
def step_impl(context):
    context.url = "http://127.0.0.1:5000/api/user/"+str(context.user_id)+"/companies/"
    context.result = requests.get(context.url, headers={'Authorization': context.auth.json().get('token')})
    print(context.payload)

@then("get company's list")
def step_impl(context):
    json_data = context.result.json()
    print(json_data)
    print(context.number_registers)
    assert len(json_data['objects']) == context.number_registers

    is_same_contract_type = True
    for object in json_data['objects']:
        if object['contract_type'] != context.contract_type:
            is_same_contract_type = False
            break
    assert is_same_contract_type


#############        END CASO 11.2  - READ          #################


#############        START CASO 11.3 - UPDATE       #################

@given('there is company')
def step_impl(context):
    context.result = requests.get("http://127.0.0.1:5000/api/companies/",
                                headers={'Authorization': context.auth.json().get('token')})
    assert len(context.result.json()['objects']) > 0


@when('editing a company')#NAO IMPLEMENTADO
def step_impl(context):
    context.model = Company
    context.id = 1
    context.old = requests.get("http://127.0.0.1:5000/api/companies/"+str(context.id)+"/",
                               headers={'Authorization': context.auth.json().get('token')})
    print(context.old.json())
    context.old_name = context.old.json()['name']

    data_edited = {'name': 'EDITED'}
    context.result = requests.put("http://127.0.0.1:5000/api/companies/"+str(context.id)+"/",
                                   headers={'Authorization': context.auth.json().get('token')},
                                   json=data_edited)
    #print('%s' % context.company.json()['objects'][0]['id'])
    context.data = {'id': context.company.json()['objects'][0]['id'], 'name': 'APPLE'}
    #assert len(context.comp.json()['x']) > 0 #só pra checar

#############       END CASO 11.3 - UPDATE          #################


#############       START CASO 11.4 - DELETE        #################

@given('there are no projects in the company')
def step_impl(context):
    context.result = requests.get("http://127.0.0.1:5000/api/company/"+str(context.id)+"/projects",
                                 headers={'Authorization': context.auth.json().get('token')})
    #print(context.result.json())
    assert len(context.result.json()['objects']) == 0

@given('there are no users in the company')
def step_impl(context):
    context.result = requests.get("http://127.0.0.1:5000/api/company/"+str(context.id)+"/users/",
                                  headers={'Authorization': context.auth.json().get('token')})
    for p in context.result.json()['objects']:
        assert p['name'] == 'Alice'
        break
    print(context.result.json())
    assert len(context.result.json()['objects']) == 1

@when('deleting the company')
def step_impl(context):
    #context.comp = requests.delete("http://127.0.0.1:5000/api/companies/"+context.id+"/",
    #                                     headers={'Authorization': context.auth.json().get('token')})
    '''for p in context.comp.json():
        if p['name'] == 'NEW':
            context.id = p['id']
    print(context.id)'''
    assert len(context.result.json()['x']) == 0     #SÓ PARA TESTE
    #context.company_delete = requests.delete("http://127.0.0.1:5000/api/company/1/",
    #                                      headers={'Authorization': context.auth.json().get('token')})

###############        END CASO 11.4 - DELETE        #################
###############          END  CRUD COMPANY           ################


##############             CRUD PROJECT                  #################
##############         START CASO 12.1 - CREATE          #################

@given('the user has a contract with the company')
def step_impl(context):
    context.contracts = requests.get("http://127.0.0.1:5000/api/company/"+str(context.company_id)+"/calendars",
                                     headers={'Authorization': context.auth.json().get('token')})
    print(context.contracts.json())
    print(context.payload)
    assert len(context.contracts.json()) > 0

@when('creating a new project in this company')
def step_impl(context):
    context.model = Assignment
    context.model_second = Project
    for comp in context.payload['companies']:
        if comp['id']:
            context.comp_id = comp['id']
    context.new = {'name': 'New Project', 'company_id': context.comp_id}
    context.result = requests.post("http://127.0.0.1:5000/api/projects/",
                                   headers={'Authorization': context.auth.json().get('token')},
                                   json=context.new)

@when('creating a new project in another company')
def step_impl(context):
    context.new = {'name': 'New Project', 'company_id': 1}
    context.result = requests.post("http://127.0.0.1:5000/api/projects/",
                                   headers={'Authorization': context.auth.json().get('token')},
                                   json=context.new)

@then("update project's list")#pegar do users/assignments
def step_impl(context):
    context.r = requests.get("http://127.0.0.1:5000/api/users/",
                                   headers={'Authorization': context.auth.json().get('token')})
    for u in context.r.json()['objects']:
        if u['name'] == context.name:
            context.user = u['id']
    #PEGA O ID DO PROJETO CRIADO E DO ASSIGNMENT PARA DEPOIS DELETAR
    context.ids = requests.get("http://127.0.0.1:5000/api/user/"+str(context.user)+"/assignments",
                               headers={'Authorization': context.auth.json().get('token')})
    for p in context.ids.json()['objects']:
        if p['project_name'] == 'New Project':
            context.id = p['id']
            context.id_second = p['project_id']

    assert context.result.json().get('name') == context.new['name']
    assert context.result.json()['company']['id'] == context.new['company_id']
    assert context.result.json()['id'] is not None

#############        END CASO 12.1 - CREATE        #################


#############        START CASO 12.2 - READ        #################

@given('there is project in the company')
def step_impl(context):
    context.result = requests.get("http://127.0.0.1:5000/api/company/"+str(context.company_id)+"/projects/",
                                   headers={'Authorization': context.auth.json().get('token')})
    assert len(context.result.json()['objects']) > 0

@when("the user goes to project's list")
def step_impl(context):
    context.list = requests.get("http://127.0.0.1:5000/api/company/1/projects/",
                                  headers={'Authorization': context.auth.json().get('token')})

@then("show project's list")
def step_impl(context):
    print(context.list.json())
    assert len(context.list.json()['objects'])>0

#############        END CASO 12.2 - READ          #################


#############       START CASO 12.3 - UPDATE       #################

@when('editing a project')
def step_impl(context):
    context.model = Project
    context.id = 1
    context.old = requests.get("http://127.0.0.1:5000/api/projects/"+str(context.id)+"/",
                                   headers={'Authorization': context.auth.json().get('token')})
    context.old_name = context.old.json()['name']
    context.old_color = context.old.json()['color']

    data_edited = {'name': 'Edited', 'color': 'black'}
    #context.result = requests.put("http://127.0.0.1:5000/api/projects/"+str(context.id)+"/",
    #                               headers={'Authorization': context.auth.json().get('token')},
    #                               json=data_edited)
    #print(context.result.json())
    assert len(context.result.json()['x']) > 0  #only for test

#############       END CASO 12.3 - UPDATE        #################


#############       START CASO 12.4 - DELETE       #################

@given('there are no users linked to the project')
def step_impl(context):
    context.result = requests.get("http://127.0.0.1:5000/api/project/"+str(context.id)+"/users/",
                                  headers={'Authorization': context.auth.json().get('token')})
    for p in context.result.json()['objects']:
        assert p['name'] == 'Alice'
        break
    assert len(context.result.json()['objects']) == 1

@given('there are no pending approvals') #CRIAR WORKENTRY NO PROJETO PARA CHECAR SE TEM PENDING E NÃO DAR UNAUTHORIZED OU NÃO
def step_impl(context):
    #context.result = requests.get("http://127.0.0.1:5000/api/project/"+str(context.id)+"/workdays/",
    #                              headers={'Authorization': context.auth.json().get('token')})
    context.result = requests.get("http://127.0.0.1:5000/api/workentries/",
                                  headers={'Authorization': context.auth.json().get('token')})
    print(context.result.json())
    assert len(context.result.json()['objects'])

@when('deleting the project')
def step_impl(context):
    context.model = Project
    #context.id = 1
    context.project = requests.delete("http://127.0.0.1:5000/api/projects/"+str(context.id)+"/",
                                   headers={'Authorization': context.auth.json().get('token')})

#############       END CASO 12.4 - DELETE      #################
#############        END CRUD PROJECT           #################


#############         START CRUD USER             #################
#############       START CASO 13.1 - CREATE      #################

@when('adding a new user')
def step_impl(context):
    context.model = User
    context.data_local = {'name': 'Victor', 'login': 'victor', 'password': 'Victor', 'email': 'Victor@abc.com'}
    context.result = requests.post("http://127.0.0.1:5000/api/users/",
                                   headers={'Authorization': context.auth.json().get('token')}, json=context.data_local)

@then("update user's list")
def step_impl(context):
    context.id = context.result.json()['id']
    assert context.result.json().get('name') == context.data_local['name']
    assert context.result.json().get('id') is not None

#############       END CASO 13.1 - CREATE       #################


#############       START CASO 13.2 - READ       #################

@when("the user goes to user's list")#PERGUNTAR DA URL
def step_impl(context):
    context.list = requests.get("http://127.0.0.1:5000/api/company/1/users/",
                                  headers={'Authorization': context.auth.json().get('token')})

@then("show user's list")
def step_impl(context):
    print(context.list.json())
    assert len(context.list.json()['objects']) > 0

#############       END CASO 13.2 - READ          #################


#############       START CASO 13.3 - UPDATE      #################

@given('there is user')
def step_impl(context):
    context.result = requests.get("http://127.0.0.1:5000/api/users/",
                                  headers={'Authorization': context.auth.json().get('token')})
    #print('%s' % context.result.json())
    assert len(context.result.json()['objects']) > 0

@when('editing user')
def step_impl(context):
    context.model = User
    context.id = 1
    context.old = requests.get("http://127.0.0.1:5000/api/users/"+str(context.id)+"/",
                                  headers={'Authorization': context.auth.json().get('token')})
    context.old_name = context.old.json()['name']
    context.old_password = context.old.json()['password']
    context.old_email = context.old.json()['email']
    #data_edited = {'name': 'Alice', 'password': 'Alice', 'email': 'Alice@abc.com'}
    data_edited = {'name': 'Escobar', 'password': 'Escobar', 'email': 'Escobar@abc.com'}
    context.result = requests.put("http://127.0.0.1:5000/api/users/"+str(context.id)+"/",
                                   headers={'Authorization': context.auth.json().get('token')},
                                   json=data_edited)
    print(context.result.status_code)

#############      END CASO 13.3 - UPDATE       #################


#############      START CASO 13.4 - DELETE     #################

@given('there is user in the company')
def step_impl(context):
    context.model = User
    context.result = requests.get("http://127.0.0.1:5000/api/project/1/users/",
                                   headers={'Authorization': context.auth.json().get('token')})
    assert len(context.result.json()['objects']) > 0

@when('unlinking user from project') #--> (REMOVE TODOS OS ASSIGNMENTS DO USER) ****CRIA ANTES E DELETA DEPOIS OU DELETA PRIMEIRO E CRIA ANTES??****
def step_impl(context):
    context.model = Assignment
    context.get = requests.get("http://127.0.0.1:5000/api/user/"+str(context.user_id)+"/assignments/",
                                   headers={'Authorization': context.auth.json().get('token')})
    for p in context.get.json()['objects'][0]:
        context.project_id = p['project_id']
        context.role_assignment = p['role']
        context.color_assignment = p['color'] #NAO IMPLEMENTADO AINDA
        context.assignment_id = p['id']

    context.unlink = requests.delete("http://127.0.0.1:5000/api/user/"+str(context.user_id)+"/assignment/",
                                   headers={'Authorization': context.auth.json().get('token')})

#############       END CASO 13.4 - DELETE      #################
#############       END CRUD - USER             #################


#############       START CRUD ASSIGNMENTS      #################
#############       START CASO 14.1 - CREATE    #################

@when('creating assignment')
def step_impl(context):
    #  CRIAR UM NOVO USUÁRIO PARA ENTÃO CRIAR UM ASSIGNMENT PARA ELE COM O MESMO PROJECT_ID E CONTRACT_ID DO USUARIO LOGADO
    context.data_user = {'name': 'Jon', 'login': 'jon', 'password': 'Jon', 'email': 'Jon@abc.com' }
    context.user = requests.post("http://127.0.0.1:5000/api/users/",
                                 headers={'Authorization': context.auth.json().get('token')},
                                 json=context.data_user)
    context.id = context.user.json()['id']

    context.data_contract = {'user_id':context.id, 'calendar_id':1, 'contract_type':'WORKER'}
    context.contract = requests.post("http://127.0.0.1:5000/api/contracts/",
                                 headers={'Authorization': context.auth.json().get('token')},
                                 json=context.data_contract)
    context.id_contract = context.contract.json()['id']

    context.data_local = {'contract_id': context.id_contract, 'project_id': 3, 'project_role': 'WORKER'}      #TERIA QUE SER NA PAGINA DO USER/ASSIGNMENTS, SENÃO ESTARIA CRIANDO UM ASSIGNMENT PRA ELE MESMO,
    context.new_assignment = requests.post("http://127.0.0.1:5000/api/user/1/assignments/",   #API/USER/ID/ASSIGN  ---> ERRO 405 METODO NÃO PERMITIDO  - PERGUNTAR
                                        headers={'Authorization': context.auth.json().get('token')},
                                        json=context.data_local)

@when("the user goes to assignment's list")
def step_impl(context):
    context.list = requests.get("http://127.0.0.1:5000/api/user/"+str(context.user_id)+"/assignments/",
                                headers={'Authorization': context.auth.json().get('token')})
    assert (context.list.json()['objects']) > 0

@when("editing assignment")
def step_impl(context):
    context.list = requests.get("http://127.0.0.1:5000/api/user/"+str(context.user_id)+"/assignments/",
                                headers={'Authorization': context.auth.json().get('token')})


@when("deleting assignment")
def step_impl(context):
    #context.list = requests.delete("http://127.0.0.1:5000/api/assignments/1/",
    #                           headers={'Authorization': context.auth.json().get('token')})
    pass





