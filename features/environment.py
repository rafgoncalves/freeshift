from datetime import datetime, timedelta
from app.models import WorkEntry, Company, User, Contract, Assignment, Project, Calendar
from config import DATABASE, SECRET_KEY
import jwt
from peewee import JOIN
import requests
db = DATABASE

def before_scenario(context, scenario):
    print("rodando before scenario")
    if 'admin_is_logged_in' in scenario.tags:
        print("admin's test")
        context.user_id = 1
        context.company_id = 1
        context.name = 'Alice'
        context.contract_type = 'ADMIN'
        context.data_local = {'username': context.name.lower(), 'password': context.name}
        context.auth = requests.post("http://127.0.0.1:5000/services/login/", json=context.data_local)
        context.json_data = context.auth.json()
        context.payload = jwt.decode(context.json_data['token'].encode(encoding='ASCII'), key=SECRET_KEY)

    elif 'worker_is_logged_in' in scenario.tags:
        print("worker's test")
        context.user_id = 2
        context.company_id = 1
        context.name = 'Bob'
        context.contract_type = 'WORKER'
        context.data_local = {'username': context.name.lower(), 'password': context.name}
        context.auth = requests.post("http://127.0.0.1:5000/services/login/", json=context.data_local)
        context.json_data = context.auth.json()
        context.payload = jwt.decode(context.json_data['token'].encode(encoding='ASCII'), key=SECRET_KEY)

    elif 'coordinator_is_logged_in' in scenario.tags:
        print("coordinator's test")
        context.user_id = 6
        context.company_id = 3
        context.name = 'Gene'
        context.data_local = {'username': context.name.lower(), 'password': context.name}
        context.auth = requests.post("http://127.0.0.1:5000/services/login/", json=context.data_local)
        context.json_data = context.auth.json()
        context.payload = jwt.decode(context.json_data['token'].encode(encoding='ASCII'), key=SECRET_KEY)

    if 'entries_context' in scenario.tags:
        print('getting entries context')
        db.connect()
        context.query_entry_user = (WorkEntry.select(WorkEntry.id,
                                                     WorkEntry.created_at,
                                                     WorkEntry.start,
                                                     WorkEntry.finish,
                                                     WorkEntry.status,
                                                     WorkEntry.assignment,
                                                     WorkEntry.obs)
                                    .join(Assignment, JOIN.LEFT_OUTER)
                                    .join(Project, JOIN.LEFT_OUTER)
                                    .join(Contract, JOIN.LEFT_OUTER, on=Assignment.contract == Contract.id)
                                    .join(User, JOIN.LEFT_OUTER)).where(Contract.user == context.user_id)

        try:
            query = context.query_entry_user.select(WorkEntry.finish).order_by(WorkEntry.finish.desc()).get()
        except WorkEntry.DoesNotExist:
            query = None
        if query is None:
            WorkEntry.create(start='2012-05-23',
                             finish='2012-05-24',
                             assignment=context.payload['assignments'][0]['assignment_id'],
                             obs='inserindo entries para finalidade de teste, pois não havia nenhuma',
                             created_at=datetime.now(),
                             status='WAITING_APPROVAL')
            WorkEntry.create(start='2008-05-23',
                             finish='2008-05-24',
                             assignment=context.payload['assignments'][0]['assignment_id'],
                             obs='inserindo entries para finalidade de teste, pois não havia nenhuma',
                             created_at=datetime.now(),
                             status='APPROVED')

        query = context.query_entry_user.select(WorkEntry.start).order_by(WorkEntry.start.asc())
        context.entry_min_date = query[0].start

        query = context.query_entry_user.select(WorkEntry.finish).order_by(WorkEntry.finish.desc())
        context.entry_max_date = query[0].finish

        db.close()

    if 'companies_context' in scenario.tags:
        print('getting companies context')
        db.connect()
        context.query_companies = (Contract.select(Company.name, Company.id.alias('company_id'), Contract.contract_type)
                                   .join(Calendar,
                                         on=(Contract.calendar == Calendar.id))
                                   .join(Company, JOIN.LEFT_OUTER,
                                         on=(Calendar.company == Company.id))
                                   .join(User, JOIN.LEFT_OUTER,
                                         on=(Contract.user == User.id))
                                   .where(User.id == context.user_id))
        db.close()
        context.number_registers = context.query_companies.count()

    if 'deleting_company' in scenario.tags:
        print("creating company to be deleted")
        db.connect()
        Company.create(name='DELL')
        context.id = Company.get(Company.name == 'DELL').id
        Contract.create(calendar=(Calendar.create(company=context.id, name='CLT', obs='DELL',
                                                  default_monday=8,
                                                  default_tuesday=8,
                                                  default_wednesday=8,
                                                  default_thursday=8,
                                                  default_friday=8,
                                                  default_saturday=8,
                                                  default_sunday=8)),
                        user=1,
                        contract_type='ADMIN')
        db.close()
    elif 'deleting_project' in scenario.tags:
        print("creating project to be deleted")
        db.connect()
        Project.create(company=1, name='Storm', color='blue')#NÃO EXISTE COLUNA DA COR AINDA, N VAI DAR CERTO
        context.id = Project.get(Project.name == 'Storm').id
        #Assignment.create(contract=1,
        #                  project=context.id,
        #                  project_role='COORDINATOR')
        db.close()
    elif 'unlinking_user' in scenario.tags:
        print("creating user to be unlinked")
        db.connect()
        Assignment.create(contract=1,
                            project=4,
                            project_role="COORDINATOR",
                            color="blue")   #NAO IMPLEMENTADO AINDA
        db.close()

    elif 'deleting_assignment' in scenario.tags:
        print("creating assignment to be deleted")
        db.connect()

        db.close()

    '''elif 'creating assignment' in scenario.tags:
        print("creating a new user and a contract for him")
        db.connect()
        User.create(name='Jon', login='jon', password='Jon', email='Jon@abc.com')
        context.id = User.get(User.name == 'Jon').id
        Contract.create(calendar=1, user=context.id, contract_type='WORKER')
        db.close()    '''



def after_scenario(context, scenario):
    if 'adding_registers' in scenario.tags:
        db.connect()
        instance = context.model.get(context.model.id == context.id)
        instance.delete_instance()
        db.close()

    elif 'adding_two_registers' in scenario.tags:
        db.connect()
         # deleta o assignment
        instance = context.model.get(context.model.id == context.id)
        instance.delete_instance()
         #deleta o project
        instance_second = context.model_second.get(context.model_second.id == context.id_second)
        instance_second.delete_instance()
        db.close()

    elif 'editing_registers' in scenario.tags:
        db.connect()
        if context.model == Company:
            company_old = Company.get(Company.id == context.id)
            company_old.name = context.old_name

        else:
            if context.model == Project:
                project_old = Project.get(Project.id == context.id)
                project_old.name = context.old_name
                project_old.color = context.old_color
            else:
                if context.model == User:
                    user = User.get(User.id == context.id)
                    user.name = context.old_name
                    user.password = context.old_password
                    user.email = context.old_email
                    user.save()
        db.close()

    elif 'deleting_workentry' in scenario.tags:
        print("creating workentry after deleting one")
        db.connect()
        if context.model == WorkEntry:
            context.model.create(start=context.entry_max_date + timedelta(days=1),
                                 finish=context.entry_max_date + timedelta(days=2),
                                 assignment=context.payload['assignments'][0]['assignment_id'],
                                 obs='inserindo depois de uma exclusão',
                                 created_at=datetime.now(),
                                 status='WAITING_APPROVAL')
        db.close()

    elif 'unlinking_user' in scenario.tags:
        print("creating user to be unlinked")
        db.connect()
        context.model.create(contract=context.user_id,
                            project=context.project_id,
                            project_role=context.role_assignment,
                            color=context.color_assignment)   #NAO IMPLEMENTADO AINDA
        db.close()




