Feature: showing off behave

# -----------    CASO 01   ---------------

  @Scenario1
  Scenario: authenticating user and alright
    Given user provides login and password ok as worker
    When user sends an authentication request
    Then message 200
    And json has token

  @Scenario1
  Scenario: authenticating user and login or password are empty
    Given user provides login but password
    When user sends an authentication request
    Then message 401

  @Scenario1
  Scenario: authenticating user and login or password are wrong
    Given user provides a wrong password
    When user sends an authentication request
    Then message 401

  @Scenario1
  Scenario: authenticating user and login or password are wrong
    Given bad request for authentication
    When user sends an authentication request
    Then message 400

# -----------    CASO 02   ---------------

  @Scenario1
  Scenario: user logging in is an admin
    Given user provides login and password ok as admin
    When user sends an authentication request
    Then message 200
    And json has token
    And user role is admin

  @Scenario1
  Scenario: user logging in is a worker
    Given user provides login and password ok as worker
    When user sends an authentication request
    Then message 200
    And json has token
    And user role is worker

  @Scenario1
  Scenario: user logging in is a coordinator
    Given user provides login and password ok as coordinator
    When user sends an authentication request
    Then message 200
    And json has token
    And user role is coordinator

# -----------    CASO 03   ---------------

  Scenario: visualization of notifications
    Given user provides login and password ok as worker
    And has notification
    When user sends an authentication request
    Then worker gets notification

  Scenario: no visualization of notifications
    Given user provides login and password ok as worker
    And does not have notification
    When user sends an authentication request
    Then worker does not get notification


# -----------    CASO 04   ---------------

  Scenario: worker replies ok to notification
    Given worker gets notification
    When worker accept project
    Then update project list
    And notification disappear

  Scenario: worker replies not ok to notification
    Given worker gets notification
    When worker does not accept project
    Then notification disappear


# -----------    CASO 05   ---------------

  @Scenario1
  @worker_is_logged_in
  @entries_context
  Scenario: visualization of entry log ok
    Given date interval ok
    When user tries to visualize entry log
    Then message 200
    And get user entry log

  @Scenario1
  @worker_is_logged_in
  @entries_context
  Scenario: visualization of empty entry log
    Given negative date interval
    When user tries to visualize entry log
    Then message 200
    And get user empty entry log

  @Scenario1
  @worker_is_logged_in
  @entries_context
  Scenario: visualization of entry log with bad date interval
    Given bad request to visualize entry log
    When user tries to visualize entry log
    Then message 400

# -----------    CASO 06   ---------------

  @Scenario1
  @worker_is_logged_in
  @entries_context
  @adding_registers
  Scenario: adding work entry ok
    Given user has assignments
    And no period conflict
    When adding entry
    Then message 201
    And log has entry

  @Scenario1
  @worker_is_logged_in
  @entries_context
  Scenario: adding work entry with period conflict
    Given user has assignments
    And period conflict
    When adding entry
    Then message 400

#  @Scenario1      Condição testada pelo frontend
#  Scenario: adding work entry with negative interval
#    Given worker is logged in
#    And entries context
#    And user has assignments
#    And negative date interval with no period conflict
#    When adding entry
#    Then message 400

# -----------    CASO 07   ---------------

  @Scenario1
  @worker_is_logged_in
  @entries_context
  Scenario: editing work entry not approved and ok
    Given entry waiting approval
    And no period conflict
    When editing entry
    Then message 202
    And log has entry

  @Scenario1
  @worker_is_logged_in
  @entries_context
  Scenario: editing work entry not approved with period conflict
    Given entry waiting approval
    And period conflict
    When editing entry
    Then message 400

  @Scenario1
  @worker_is_logged_in
  @entries_context
  Scenario: editing work entry approved
    Given entry approved
    And no period conflict
    When editing entry
    Then message 400

# -----------    CASO 08   ---------------

  @Scenario1
  @worker_is_logged_in
  @entries_context
  @deleting_workentry
  Scenario: deleting work entry not approved
    Given entry waiting approval
    When deleting entry
    Then message 204
    And log does not have entry

  @Scenario1
  @worker_is_logged_in
  @entries_context
  Scenario: deleting work entry that does not exist
    Given entry does not exist
    When deleting entry
    Then message 404

  @Scenario1
  @worker_is_logged_in
  @entries_context
  Scenario: deleting work entry approved
    Given entry approved
    When deleting entry
    Then message 400

# -----------    CASO 09   ---------------

  # (Given entries context) sendo executado em cada teste para evitar um contexto antigo
  @Scenario1
  @admin_is_logged_in
  Scenario: admin extends worker
    #Given admin is logged in
    When execute all workers tests
    Then no test fails

# -----------    CASO 10   ---------------

  @admin_is_logged_in
  Scenario: admin wants to visualize his workers entry log
    #Given admin is logged in
    When admin tries to visualize his workers entry log
    Then admin gets his workers entry log and only them

  @admin_is_logged_in
  Scenario: admin wants to approve his workers entry log
    Given admin can visualize his workers entry log
    When admin tries to approve or disapprove his workers entry work
    Then update list of pending approval
    And update entry status



# ---------     CRUD COMPANY  CASO 11   -----------

# ---------     CRUD COMPANY     -----------


  @caso11.1
  @admin_is_logged_in
  @adding_registers
  Scenario: creating a new company
    When creating a company
    Then message 201
    And update company's list

  @caso11.2
  @admin_is_logged_in
  @companies_context
  Scenario: visualization of company's list as admin
    When user tries to visualize company's list
    Then message 200
    And get company's list

  @caso11.2
  @worker_is_logged_in
  @companies_context
  Scenario: visualization of company's list as worker
    When user tries to visualize company's list
    Then message 200
    And get company's list

  @caso11.3
  @admin_is_logged_in
  #@editing_registers
  Scenario: editing a company
    Given there is company
    When editing a company
    Then message 204

  @caso11.4
  @admin_is_logged_in
  #@deleting_company
  Scenario: deleting a company
    Given there is company
    And there are no projects in the company
    And there are no users in the company
    When deleting the company
    Then message 204


#  ---------     CRUD PROJECT  CASO 12   ------------

#  ---------     CRUD PROJECT     ------------


  @caso12.1
  @admin_is_logged_in
  @adding_two_registers
  Scenario: creating a new project as admin
    Given the user has a contract with the company
    When creating a new project in this company
    Then update project's list
    And message 201

  @caso12.1.2
  @coordinator_is_logged_in
  @adding_two_registers
  Scenario: creating a new project as coordinator
    Given the user has a contract with the company
    When creating a new project in this company
    Then update project's list
    And message 201

  @caso12.1.3
  @coordinator_is_logged_in
  Scenario: creating a new project as coordinator of another company
    Given the user has a contract with the company
    When creating a new project in another company
    Then message 401

  @caso12.1.4
  @worker_is_logged_in
  Scenario: creating a new project as worker
    Given the user has a contract with the company
    When creating a new project in this company
    Then message 401

  @caso12.2
  @admin_is_logged_in
  Scenario: viewing project's list
    Given there is company
    And there is project in the company
    When the user goes to project's list
    Then show project's list
    And message 200

  @caso12.3
  @admin_is_logged_in
  #@editing_registers
  Scenario: editing a project
    Given there is project in the company
    When editing a project
    Then message 204

  @caso12.4 #COR NÃO IMPLEMENTADO, ESPERAR PARA TESTAR
  @admin_is_logged_in
  #@deleting_project
  Scenario: deleting a project
    Given there are no users linked to the project
    And there are no pending approvals
    When deleting the project
    Then message 204


#--- ------------    CRUD USER  CASO 13   ------------------

#--- ------------    CRUD USER     ------------------

  @caso13.1
  @admin_is_logged_in
  @adding_registers
  Scenario: adding a new user to a company
    Given there is company
    When adding a new user
    Then update user's list
    And message 201

  @caso13.2
  @admin_is_logged_in
  Scenario: viewing user's list
    Given there is company
    And there is project in the company
    And there is user
    When the user goes to user's list
    Then show user's list
    And message 200

  @caso13.3
  @admin_is_logged_in
  @editing_registers
  Scenario: editing user
    Given there is user
    When editing user
    Then message 204

  @caso13.4.1   # CRIAR O ASSIGNMENT ANTES E DELETAR DEPOIS OU O CONTRARRIO?
  @admin_is_logged_in
  #@unlinking_user   REMOVER ASSIGNMENT DO USER
  Scenario: unlinking an user from project
    Given there is company
    And there is project in the company
    And the user has a contract with the company
    When unlinking user from project
    Then message 204

  @caso13.4.2
  @admin_is_logged_in
  #@unlinking_user    REMOVER CONTRACT, ASSIGNMENTE ETC  -  NÃO FAZER AINDA, MUITA COISA E NÃO IMPLEMENTADO
  Scenario: unlinking an user from company
    Given there is company
    And there is project in the company
    And there is user in the company
    When unlinking user
    Then message 204


# ----------   CRUD ASSIGNMENT  CASO 14   ---------------

# ----------   CRUD ASSIGNMENT    ---------------

  @caso14.1
  @admin_is_logged_in
  Scenario: creating an assignment
    Given there is project in the company
    When creating assignment
    Then message 201

  @caso14.1.2
  @coordinator_is_logged_in
  Scenario: creating an assignment
    Given there is project in the company
    When creating assignment
    Then message 201

  @caso14.1.2
  @worker_is_logged_in
  Scenario: creating an assignment
    Given there is project in the company
    When creating assignment
    Then message 401

  @caso14.2
  @admin_is_logged_in
  Scenario: viewing assignment's list
    Given there is project in the company
    And there is assignment
    When the user goes to assignment's list
    And message 200

  @caso14.3
  @admin_is_logged_in
  Scenario: editing an assignment
    Given there is project in the company
    And there is assignment
    When editing assignment
    Then message 204

  @caso14.4
  @admin_is_logged_in
  @deleting_assignment
  Scenario: deleting an assignment
    Given there is assignment in the company
    When deleting assignment
    Then message 204







