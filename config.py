import os
from playhouse.postgres_ext import PostgresqlExtDatabase
import pwd

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
if pwd.getpwuid(os.getuid()).pw_name == 'ubuntu':
    DATABASE = PostgresqlExtDatabase(
        'freischeisse',
        user='freischeisse',
        password='MoDyfsZwgCKR5cTM3nCq',
        host='freeshift.crovuqww3tw6.us-east-1.rds.amazonaws.com',
        port='5432'
    )
else:
    DATABASE = PostgresqlExtDatabase(
        'freischeisse', user='freischeisse', password='BEDmg4RTwqzcyfouu3X8', host='localhost', port='5432'
    )
DEBUG = True
SECRET_KEY = "secret"
STATIC_FOLDER = os.path.join(BASE_DIR, 'app', 'static')
STATIC_URL_PATH = '/app'
