angular.module('hourBank.users').controller('WorklogCtrl',  function ($modal, $rootScope, $scope, WorkLogService, worklogModalModel, $localStorage, UserData, $state, shareService, hoursIntervalService, worklogBlockConstructorService, logOutService, stringUtils, MainDashboardResourceService, dateUtils, popoverManager) {
	var openedPopover;

	$scope.open = function($event, $log) {
	    $event.preventDefault();
	    $event.stopPropagation();
	    
	    $scope.opened = true;
    };
    
    $scope.openDataFinal = function($event, $log) {
	    $event.preventDefault();
	    $event.stopPropagation();
	    
	    $scope.openedDataFinal = true;
    };

    var saveEntries = function(){
    	UserData.removeAllWorkentries();
        for ( i = 0; i < $scope.days.length; ++i){
        	UserData.addAllWorkentries($scope.days[i].work_entries);
        }  
    }

	var getHoursInterval = function(){
		var interval =hoursIntervalService.getHoursInterval($scope.days);
		$scope.startHour = interval.minStart;
		$scope.endHour = interval.maxEnd;
		$scope.hourWidth = interval.hourWidth;
		$scope.hours = interval.hours;
	}

	var worklogBlockContructor = function() {
		$scope.timeBlocks = [];
		for (var i = 0; i < $scope.days.length; i++){
			var blocks = worklogBlockConstructorService.getTimeBlocks($scope.days[i].work_entries, $scope.startHour, $scope.endHour, $scope.hourWidth);
			$scope.timeBlocks.push({date : stringUtils.convertDateToString($scope.days[i].date), blocks : blocks});
		}
	}

	var getWorkDays = function(){
      var n_start_date = dateUtils.subtractDays($scope.dateSelected, 1);
      var n_end_date = dateUtils.addDays($scope.dateFinalSelected, 1);
      query = {start_date: n_start_date, end_date: n_end_date, token:UserData.getToken()}
      MainDashboardResourceService.getWorkDays(query).then(function(data) {
        if(data.status == "200"){
        	$scope.days = data.data.objects;
    	    $scope.days = hoursIntervalService.normalizeInterval($scope.days);
   			getHoursInterval();
	        saveEntries();
			worklogBlockContructor();
        }
      }, function(data){
        if (data.status="403"){
          UserData.clear();
          $state.go("login");
        }
      });
	};


	$scope.setCreationMode = function(date) {
		var popovers = $('.popover');
		openedPopover = popoverManager.hidePopover(popovers);
		worklogModalModel.mode = "CREATION";
		if (date === 'new'){
			worklogModalModel.setDate(new Date());
		}else{
			worklogModalModel.setDate(dateUtils.addDays(date, 1));
		}
	}

	$scope.setModalData = function(date, id) {
		if(id){
			var popovers = $('.popover');
			openedPopover = popoverManager.hideExceedingPopover(openedPopover, popovers);
			worklogModalModel.id = id;
			worklogModalModel.mode = "UPDATE";
			worklogModalModel.setDate(date);
			$rootScope.$broadcast('worklog-selected', id, date);
		}
	}

	$scope.loadDates = function(){
		if ($scope.dateSelected <= $scope.dateFinalSelected){
			getWorkDays();	
		}
	}

	$scope.logOut = function(){
		logOutService.logOut();
	}

	$scope.$on('log-changed', function(scope) {
		getWorkDays();
	});


	$scope.checkProject = function(){
		if ($scope.assignmentSelected){
			if($scope.assignmentSelected.project_role === "ADMIN" || $scope.assignmentSelected.project_role === "COORDINATOR"){
				shareService.setAssignmentSelected($scope.assignmentSelected);
				$state.go("user.checkProject");
			}else{
				alertify.error("Acesso negado.");
			}
		}
	};


	$scope.createProject = function(){
		$state.go("user.createProject");
	};

	$scope.createCalendar = function(){
		$state.go("user.createCalendar");
	};

	$scope.createAssignment = function(){
		$state.go("user.createAssignment");
	};

	$scope.checkCompany = function(){
		if ($scope.companySelected){
			if ($scope.companySelected.role === "ADMIN" || $scope.companySelected.role === "COORDINATOR"){
				shareService.setCompanySelected($scope.companySelected);
				$state.go("user.checkCompany");
			}else{
				alertify.error("Acesso negado.");
			}
		}
	};

	var initController = function(){
		$scope.format = 'dd/MM/yyyy';
		$scope.dateSelected = new Date();
		$scope.dateFinalSelected = new Date();
	    $scope.dateSelected  = dateUtils.subtractDays($scope.dateSelected, 1);
	    $scope.dateFinalSelected = dateUtils.addDays($scope.dateFinalSelected, 1);
		$scope.usernameLogado = UserData.getUsername();
		$scope.dateOptions = {
		   formatYear: 'yy',
		   startingDay: 1
		};
		$scope.assignments = UserData.getAssignments();
		$scope.companies = UserData.getCompanies();
		$scope.companySelected = "";
		$scope.days =  "";
		openedPopover = "";
		getWorkDays();

	};
	initController();
});