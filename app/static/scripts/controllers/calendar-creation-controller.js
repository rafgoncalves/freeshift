angular.module('hourBank.users').controller('CalendarCreationController', function ($scope, UserData, assignmentModalModel, CalendarCRUDService, $state, logOutService) {
	
	var newCalendar = function(){
		var calendar = [];
		calendar.horas = $scope.horas;
		calendar.name = $scope.chosenName;
		calendar.company_id = $scope.selectedCompany.id;
		calendar.observation = $scope.observation;
		console.log(calendar);
		CalendarCRUDService.createCalendar(calendar, UserData.getToken()).then(function(data){
       		if(data.status == "201"){
       			alertify.success("Calendário " + calendar.name + " criado com sucesso!"); //Retirar mensagem após implementação do redirecionamento para a tela de assignment.
       			$state.go("user.dashboard"); //Após criação da tela de assignment, redirecionar para esta tela.
        	}		
      	}, function(data){
       		alertify.error(data.data.error);
      	});
	};

	var completelyFilled = function(collection){
		for(item in collection){
			if (!($.isNumeric(collection[item]))){
				return false;
			}
		}
		return true;
	};

	var validate = function(){
	   if ( (!$scope.chosenName) || (!$scope.selectedCompany) || !(completelyFilled($scope.horas)) ){
	     return "Preencha o nome, horas de trabalho e a empresa do calendário.";
	   }
	   return "";
	};

	$scope.createCalendar = function(){
		$scope.invalidMessage = validate();
		if($scope.invalidMessage === ""){
			newCalendar();
		}
	}

	
	$scope.logOut = function(){
		logOutService.logOut();
	}
	
	$scope.voltar = function(){
		$state.go("user.dashboard");
	};

	var initController = function(){
	 	$scope.companies = UserData.getCompanies();
	 	$scope.usernameLogado = UserData.getUsername();
	 	$scope.chosenName = "";
	 	$scope.selectedCompany = "";
	 	$scope.invalidMessage = "";
	 	$scope.horas={};
	 	$scope.horas.segunda=8;
	 	$scope.horas.terca=8;
	 	$scope.horas.quarta=8;
	 	$scope.horas.quinta=8;
	 	$scope.horas.sexta=8;
	 	$scope.horas.sabado=0;
	 	$scope.horas.domingo=0;
	};
	initController();
});