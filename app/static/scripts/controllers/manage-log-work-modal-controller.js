angular.module('hourBank.users').controller('manageLogWorkModalCtrl', function ($rootScope, $scope, WorkLogService, worklogModalModel, UserData, stringUtils) {


  $scope.$on('manage-selected-log', function(scope, id, date) {
    loadWorklogEntry(id, date);
  });

  var loadWorklogEntry = function(id, date){
    $scope.worklogSelected =  UserData.getWorkentryById(id);
    
    $scope.worklog.assignment.project_name = $scope.worklogSelected.project;
    $scope.observations = $scope.worklogSelected.obs;
    $scope.date = date;
    $scope.worklog.user = $scope.worklogSelected.user;
    $scope.worklog.status = translateStatus($scope.worklogSelected.status);
    $scope.worklog.id = id;
    
    loadTimeValues();
  };

  var loadTimeValues = function(){
    $scope.startHour = stringUtils.sliceHour($scope.worklogSelected.start);
    $scope.startMinute = stringUtils.sliceMinute($scope.worklogSelected.start);
    $scope.finishHour = stringUtils.sliceHour($scope.worklogSelected.finish);   
    $scope.finishMinute = stringUtils.sliceMinute($scope.worklogSelected.finish);
  };

  $scope.disapproveWorklog = function(){
    manageLog(false);
  };

  $scope.approveWorklog = function(){
    manageLog(true);
  };

   var manageLog = function(approval){
        WorkLogService.manageWorklog($scope.worklog.id, approval, UserData.getToken()).then(function(data){
        if(data.status == "202"){
          $('#manage-log-modal').modal('hide');
          $rootScope.$broadcast('log-managed');
        }
      }, function(data){
        alertify.error(data.data.error);
      });
  };

  var translateStatus = function(status){
      var translation = "";
      if (status === "APPROVED"){
          translation = "Aprovado";
      }else if (status === "WAITING_APPROVAL"){
          translation = "Aguardando aprovação";
      }else if (status === "REPROVED"){
          translation = "Reprovado";
      } 
      return translation;
  };
  var initController = function(){
    $scope.worklog = [];
    $scope.worklog.assignment = [];
    $scope.worklog.assignment.project_name="";
    $scope.worklogSelected = [];
  };
  initController();

});