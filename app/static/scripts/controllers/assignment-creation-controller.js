angular.module('hourBank.users').controller('AssignmentCreationController', function ($state, $scope, UserData, assignmentModalModel, AssignmentCRUDService, ProjectResourcesService, logOutService) {
	var newAssignment = function(){
		assignmentModalModel.role = $scope.roleSelected;
		assignmentModalModel.user = UserData.getUsername();
		var p = eval($scope.projetoSelected);
		assignmentModalModel.project  = p.project_name;
		assignmentModalModel.company = p.company_name;
		AssignmentCRUDService.newAssignment(assignmentModalModel, UserData.getToken()).then(function(data){
       		if(data.status == "201"){
       			alertify.success("Assignment criado com sucesso!"); 
          		$state.go("user.dashBoard");
        	}		
      	}, function(data){
       		alertify.error(data.data.error);
      	});
	};


	var getProjectWorkers = function(id){
		var query = {token : UserData.getToken(), projectId : id};
		ProjectResourcesService.getProjectWorkers(query).then(function(promise){
		    if (promise.status === 200){
		  		$scope.opcoesFuncionarios = promise.data.objects;
		    }
		}, function(promise){
		});
	};

	var initController = function(){
		$scope.roles = [];
		$scope.roles.push("WORKER");
		$scope.roles.push("COORDINATOR");
	 	$scope.projetos = UserData.getAssignments();
	 	$scope.invalidMessage = "";
	 	$scope.usernameLogado = UserData.getUsername();

		$scope.assignments = [];
	};
	initController();

	$scope.loadUsersFromProject = function(){
		if ($scope.projetoSelected){
			getProjectWorkers($scope.projetoSelected.project_id);
		}else{
			$scope.opcoesFuncionarios = [];
		}
		$scope.funcionarioSelected = null;
	};

	var containsAssignment = function(idProjeto, pessoa, role){
		var i=0;
		for(i=0;i<$scope.assignments.length;i++){
			if (idProjeto === $scope.assignments[i].projeto.project_id && pessoa === $scope.assignments[i].pessoa && role === $scope.assignments[i].role){
				return true;
			}
		}
		return false;
	};

	var oppositeRole = function(role){
		if (role === "COORDINATOR"){
			return "WORKER";
		}

		return "COORDINATOR";
	}
	var validate = function(projeto, pessoa, role){
		if (!projeto || !pessoa || !role){
			return "Preencha projeto, funcionário e cargo.";			
		}

		if(containsAssignment(projeto.project_id, pessoa, role)){
			return "Registro já está presente na lista de assignments a serem criados.";
		}
		var r = oppositeRole(role);
		if(containsAssignment(projeto.project_id, pessoa, r)){
			return pessoa.name + " já possui o cargo " + r + " no projeto " + projeto.project_name + ".";
		}

		return "";
	}

	$scope.add = function(){
		$scope.invalidMessage = validate($scope.projetoSelected, $scope.funcionarioSelected, $scope.roleSelected);
		if ($scope.invalidMessage === ""){
			var projeto = new Object();
			projeto.project_name = $scope.projetoSelected.project_name;
			projeto.project_id = $scope.projetoSelected.project_id;
			projeto.company_name = $scope.projetoSelected.company_name;
			$scope.assignments.push({'projeto' : projeto, 'pessoa' : $scope.funcionarioSelected, 'role' : $scope.roleSelected});
		}
	};

	$scope.remove = function(elementIndex){
		var newArray = [];
		for(i=0;i<$scope.assignments.length;i++){
			if (i != elementIndex){
				newArray.push($scope.assignments[i]);
			}
		}
		$scope.assignments=newArray;
	};

	$scope.logOut = function(){
		logOutService.logOut();
	}
	
	$scope.voltar = function(){
		$state.go("user.dashboard");
	};


	$scope.createAssignment = function(){
		console.log($scope.assignments);
	};

});