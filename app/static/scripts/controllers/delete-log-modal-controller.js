angular.module('hourBank.users').controller('deleteLogWorkModalCtrl', function (stringUtils, $rootScope, $scope, WorkLogService, worklogModalModel, UserData) {
	$scope.worklog = [];
	$scope.worklog.assignment = [];
	$scope.worklog.assignment.project_name="Project";
	$scope.worklogSelected = [];
	$scope.reloadDate;
	$scope.$on('worklog-selected', function(scope, id, date) {
		loadWorklogEntry(id, date);
	});
	



	var loadWorklogEntry = function(id, date){
		$scope.worklogSelected =  UserData.getWorkentryById(id);
		
		$scope.worklog.assignment.project_name = $scope.worklogSelected.project;
		$scope.observations = $scope.worklogSelected.obs;
		$scope.date = date;
		
		loadTimeValues();
	};

	var loadTimeValues = function(){
		$scope.startHour = stringUtils.sliceHour($scope.worklogSelected.start);
		$scope.startMinute = stringUtils.sliceMinute($scope.worklogSelected.start);
		$scope.finishHour = stringUtils.sliceHour($scope.worklogSelected.finish);		
		$scope.finishMinute = stringUtils.sliceMinute($scope.worklogSelected.finish);
	};

	$scope.deleteWorklog = function(){
		WorkLogService.deleteWorklog($scope.worklogSelected.id, UserData.getToken()).then(function(data){
        if(data.status == "204"){
	     		$rootScope.$broadcast('log-changed');
	     		$('#delete-log-modal').modal('hide');
        }
      	}, function(data){
        	alertify.error(data.data.error);
     	});

	};


});