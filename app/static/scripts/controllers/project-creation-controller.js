angular.module('hourBank.users').controller('ProjectCreationController', function ($scope, UserData, assignmentModalModel, ProjectCRUDService, $state, logOutService) {
	
	var newProject = function(){
		var project = [];
		project.color = $scope.selectedColor;
		project.name = $scope.chosenName;
		project.company_id = $scope.selectedCompany.id;
		ProjectCRUDService.createProject(project, UserData.getToken()).then(function(data){
       		if(data.status == "201"){
       			alertify.success("Projeto " + project.name + " criado com sucesso!"); //Retirar mensagem após implementação do redirecionamento para a tela de assignment.
       			$state.go("user.dashboard"); //Após criação da tela de assignment, redirecionar para esta tela.
        	}		
      	}, function(data){
       		alertify.error(data.data.error);
      	});
	};

	var validate = function(){
	   if ( (!$scope.chosenName) || (!$scope.selectedCompany) || (!$scope.selectedColor)){
	     return "Preencha o nome, a cor e a empresa do projeto.";
	   }
	   return "";
	};

	$scope.createProject = function(){
		$scope.invalidMessage = validate();
		if($scope.invalidMessage === ""){
			newProject();
		}
	}

	
	$scope.logOut = function(){
		logOutService.logOut();
	}
	
	$scope.voltar = function(){
		$state.go("user.dashboard");
	};

	var initController = function(){
	 	$scope.companies = UserData.getCompanies();
	 	$scope.usernameLogado = UserData.getUsername();
	 	$scope.chosenName = "";
	 	$scope.selectedCompany = "";
	 	$scope.selectedColor = "#000000";
	 	$scope.invalidMessage = "";
	};
	initController();
});