angular.module('hourBank.users').controller('ProjectDashboardController',  function ($rootScope, $scope, ProjectResourcesService, projectDayModel, UserData, $state, shareService, hoursIntervalService, worklogBlockConstructorService, logOutService, projectDaysMiddleware, dateUtils) {

	/*
		Obtém os blocos a serem exibidos na tela.
	*/
	var getWorkBlocks = function(){
		UserData.removeAllWorkentries();
  	  	for(var i=0; i<$scope.dateEntries.length; i++){
  	  		for(var j=0; j<$scope.dateEntries[i].userData.length; j++){
  	  			var blocks = worklogBlockConstructorService.getTimeBlocks($scope.dateEntries[i].userData[j].work_logs, $scope.startHour, $scope.endHour, $scope.hourWidth);
  	  			$scope.dateEntries[i].userData[j].blocks = blocks;
  	  			UserData.addAllWorkentries($scope.dateEntries[i].userData[j].work_logs);
  	  		}
  	  	}
	}	
	/*
		Obtém o intervalo de horas a ser exibido.
	*/
	var getHoursInterval = function(){
		var interval = hoursIntervalService.getHoursInterval($scope.days);
		$scope.startHour = interval.minStart;
		$scope.endHour = interval.maxEnd;
		$scope.hourWidth = interval.hourWidth;
		$scope.hours = interval.hours;
	};

	/*
		Obtém a lista de funcionários cadastrados em um projeto.
	*/
	var getProjectWorkers = function(id){
		var query = {token : UserData.getToken(), projectId : id};
		ProjectResourcesService.getProjectWorkers(query).then(function(promise){
		    if (promise.status === 200){
		  		$scope.opcoesFuncionarios = promise.data.objects;
		    }
		}, function(promise){
		});
	};
	/*
		Obtém os logs dos funcionários de um projeto.
	*/
    var getProjectDays = function(pId, uId){
    	var n_start_date = dateUtils.subtractDays($scope.diaInicialSelected, 1);
	    var n_end_date = dateUtils.addDays($scope.diaFinalSelected, 1);
    	var query = {token : UserData.getToken(), projectId : pId, start_date : n_start_date, end_date : n_end_date, userId : uId, waiting_approval : $scope.waitingApproval};
    	ProjectResourcesService.getWorkDays(query).then(function(promise){
		    if (promise.status === 200){
		  		$scope.days = promise.data.objects;
				$scope.days = hoursIntervalService.normalizeInterval($scope.days);
				$scope.dateEntries = projectDaysMiddleware.splitEntries($scope.days);
		  	  	getHoursInterval();
		  	  	getWorkBlocks();
		    }
		}, function(promise){
			alertify.error(promise.data.error);
			if (promise.status === 401){
				$state.go("user.dashboard");
			}

		});

    };



	/*
		Inicializações
	*/
	var initController = function(){
		$scope.days = "";
		$scope.format = 'dd/MM/yyyy';
		$scope.diaInicialSelected = new Date();
		$scope.diaFinalSelected = new Date();
	    $scope.diaInicialSelected  = dateUtils.subtractDays($scope.diaInicialSelected, 3);
	    $scope.diaFinalSelected = dateUtils.addDays($scope.diaFinalSelected, 3);
	    $scope.opcoesFuncionarios = [];
	    $scope.funcionarioSelected = null;
	    $scope.usernameLogado = UserData.getUsername();
		$scope.dateOptions = {
		   formatYear: 'yy',
		   startingDay: 1
		};
		$scope.assignment = shareService.getAssignmentSelected();
		$scope.waitingApproval = false;
		getProjectDays($scope.assignment.project_id, null);
		getProjectWorkers($scope.assignment.project_id);
	};
	initController();



	/*
		Filtra os logs de acordo com o que foi selecionado na tela.
	*/
	$scope.filter = function(){
		getProjectDays($scope.assignment.project_id, $scope.funcionarioSelected);
	};

    /*
		Trata os eventos de seleção de uma nova data de filtro.
    */
    $scope.open = function($event, $log) {
	    $event.preventDefault();
	    $event.stopPropagation();
	    
	    $scope.opened = true;
    };
    $scope.openDataFinal = function($event, $log) {
	    $event.preventDefault();
	    $event.stopPropagation();
	    
	    $scope.openedDataFinal = true;
    };


    /*
		Volta para o dashboard principal.
	*/
	$scope.voltar = function(){
		$state.go("user.dashboard");
	};

    /*
    	Logout
    */
	$scope.logOut = function(){
		logOutService.logOut();
	};

	/*
		Faz com que o modal de gerenciamento de worklog tenha seus dados preenchidos.
	*/
	$scope.loadLogManagement = function(id, date){
		if(id){
			$rootScope.$broadcast('manage-selected-log', id, date);
		}
	}

	/*
		Recarrega a página quando há aprovação ou desaprovação de um worklog.
	*/
	$scope.$on('log-managed', function(scope, id, date) {
    	$scope.filter();
  	});



});