angular.module('hourBank.users').controller('CompanyDashboardController',  function ($rootScope, $scope, CompanyResourcesService, projectDayModel, UserData, $state, shareService, hoursIntervalService, worklogBlockConstructorService, logOutService, companyDaysMiddleware, dateUtils) {


	/*
		Obtém os blocos a serem exibidos na tela.
	*/
	var getWorkBlocks = function(){
		UserData.removeAllWorkentries();
  	  	for(var i=0; i<$scope.dateEntries.length; i++){
  	  		for(var j=0; j<$scope.dateEntries[i].projectData.length; j++){
  	  			for(var k=0; k<$scope.dateEntries[i].projectData[j].userData.length; k++){
  	  				var blocks = worklogBlockConstructorService.getTimeBlocks($scope.dateEntries[i].projectData[j].userData[k].work_entries, $scope.startHour, $scope.endHour, $scope.hourWidth);
  	  				$scope.dateEntries[i].projectData[j].userData[k].work_blocks = blocks;
  	  				UserData.addAllWorkentries($scope.dateEntries[i].projectData[j].userData[k].work_entries);
  	  			}
	  	  	}
  	  	}		
	}
	/*
		Obtém o intervalo de horas a ser exibido.
	*/
	var getHoursInterval = function(){
		var interval = hoursIntervalService.getHoursInterval($scope.days);
		$scope.startHour = interval.minStart;
		$scope.endHour = interval.maxEnd;
		$scope.hourWidth = interval.hourWidth;
		$scope.hours = interval.hours;
	};

	/*
		Obtém os projetos de uma empresa.
	*/
	var getCompanyProjects = function(id){
		var query = {token : UserData.getToken(), companyId : id};
		CompanyResourcesService.getCompanyProjects(query).then(function(promise){
		    if (promise.status === 200){
		  		$scope.opcoesProjetos = promise.data.objects;
		    }
		}, function(promise){
		});
	};


	/*
		Obtém os funcionários de uma empresa.
	*/
	var getCompanyWorkers = function(id){
		var query = {token : UserData.getToken(), companyId : id};
		CompanyResourcesService.getCompanyWorkers(query).then(function(promise){
		    if (promise.status === 200){
		  		$scope.opcoesFuncionarios = promise.data.objects;
		    }
		}, function(promise){
		});
	};


	/*
		Obtém os logs dos funcionários de uma empresa.
	*/
    var getWorkDays = function(cId, pId, uId){
    	var n_start_date = dateUtils.subtractDays($scope.diaInicialSelected, 1);
	    var n_end_date = dateUtils.addDays($scope.diaFinalSelected, 1);
    	var query = {token : UserData.getToken(), projectId : pId, start_date : n_start_date, end_date : n_end_date, userId : uId, companyId: cId, waiting_approval : $scope.waitingApproval};
    	CompanyResourcesService.getWorkDays(query).then(function(promise){
		    if (promise.status === 200){
		  		$scope.days = promise.data.objects;
				$scope.days = hoursIntervalService.normalizeInterval($scope.days);
				$scope.dateEntries = companyDaysMiddleware.splitEntries($scope.days);
		  	  	getHoursInterval();
		  	  	getWorkBlocks();

		    }
		}, function(promise){
			alertify.error(promise.data.error);
			if (promise.status === 401){
				$state.go("user.dashboard");
			}

		});

    };
	/*
		Inicializações
	*/
	var initController = function(){
		$scope.days = "";
		$scope.format = 'dd/MM/yyyy';
		$scope.diaInicialSelected = new Date();
		$scope.diaFinalSelected = new Date();
	    $scope.diaInicialSelected  = dateUtils.subtractDays($scope.diaInicialSelected, 3);
	    $scope.diaFinalSelected = dateUtils.addDays($scope.diaFinalSelected, 3);
	    $scope.opcoesFuncionarios = [];
	    $scope.funcionarioSelected = null;
	    $scope.opcoesProjetos = [];
	    $scope.projetoSelected = null;
	    $scope.usernameLogado = UserData.getUsername();
		$scope.dateOptions = {
		   formatYear: 'yy',
		   startingDay: 1
		};
		$scope.company = shareService.getCompanySelected();
		getWorkDays($scope.company.id, null, null);
		getCompanyProjects($scope.company.id);
		getCompanyWorkers($scope.company.id);
	};
	initController();

	/*
		Filtra os logs de acordo com o que foi selecionado na tela.
	*/
	$scope.filter = function(){
		getWorkDays($scope.company.id, $scope.projetoSelected, $scope.funcionarioSelected);
	};

    /*
		Trata os eventos de seleção de uma nova data de filtro.
    */
    $scope.open = function($event, $log) {
	    $event.preventDefault();
	    $event.stopPropagation();
	    
	    $scope.opened = true;
    };
    $scope.openDataFinal = function($event, $log) {
	    $event.preventDefault();
	    $event.stopPropagation();
	    
	    $scope.openedDataFinal = true;
    };

    /*
		Volta para o dashboard principal.
	*/
	$scope.voltar = function(){
		$state.go("user.dashboard");
	};

    /*
    	Logout
    */
	$scope.logOut = function(){
		logOutService.logOut();
	};

	/*
		Faz com que o modal de gerenciamento de worklog tenha seus dados preenchidos.
	*/
	$scope.loadLogManagement = function(id, date){
		if(id){
			$rootScope.$broadcast('manage-selected-log', id, date);
		}
	}

	/*
		Recarrega a página quando há aprovação ou desaprovação de um worklog.
	*/
	$scope.$on('log-managed', function(scope, id, date) {
    	$scope.filter();
  	});
  	
});