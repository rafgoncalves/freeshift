angular.module('hourBank.login')
	.controller('LoginController', function($scope, $window, loginService, $http, $rootScope,$state, UserData, jwtHelper) {  
	    $scope.attemptLogin = {
	        username : "",
			password : ""
	    };

		$scope.doLogin = function(){

				if (($scope.attemptLogin.username != "") && ($scope.attemptLogin.password != "")){
					loginService.autenticate( $http, $scope.attemptLogin.username, $scope.attemptLogin.password).then(function(promise){
						  if (promise.status === 200){
						  	var decodedToken = jwtHelper.decodeToken(promise.data.token);

							UserData.setUsername(decodedToken.name);
							UserData.setAssignments(decodedToken.assignments);
							UserData.setCompanies(decodedToken.companies);
							UserData.setToken(promise.data.token);
							$scope.invalidLogin = false;
							$state.go("user.dashboard");
						  }
					}, function(promise){
						  if (promise.status === 401){
							$scope.invalidLogin = true;
						  }				
					});
						
				}
			
			
		};
});
