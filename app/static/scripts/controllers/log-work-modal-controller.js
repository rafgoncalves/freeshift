angular.module('hourBank.users').controller('LogWorkModalCtrl', function ($rootScope, $scope, WorkLogService, worklogModalModel, UserData, stringUtils, dateUtils) {

  // Disable weekend selection
  $scope.disabled = function(date, mode) {
    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
  };

  $scope.open = function($event, $log) {
    $event.preventDefault();
    $event.stopPropagation();
    
    $scope.opened = true;
  };


  $scope.today = function() {
    $scope.date = new Date();

  };


  var initModalController = function(){
    if (worklogModalModel.mode === "CREATION"){
        $scope.startTime = worklogModalModel.startTime;
        $scope.endTime = worklogModalModel.endTime;
        $scope.observation = worklogModalModel.observation;
        $scope.title = "Criação";
        $scope.logId = "";
    }else if (worklogModalModel.mode === "UPDATE"){
        var logSelected  =  UserData.getWorkentryById(worklogModalModel.id);
        $scope.logId = logSelected.id;
        $scope.assignmentSelected = logSelected.assignment_id;
        $scope.observation = logSelected.obs;
        $scope.startTime = dateUtils.convertHourToDate(logSelected.start);
        $scope.endTime = dateUtils.convertHourToDate(logSelected.finish);
        $scope.title = "Alteração";
    }
    $scope.date = worklogModalModel.date;
    $scope.invalidMessage = "";
    $scope.invalidLogin = false;  
    $scope.hstep = 1;
    $scope.mstep = 1;
    $scope.ismeridian = true;
    $scope.assignments = UserData.getAssignments();
    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };
    $scope.format = 'dd/MM/yyyy';
  }
  
  var validate = function(){
    if ( (!$scope.date) || (!$scope.startTime) || (!$scope.endTime) || (!$scope.assignmentSelected) ){
      return "Preencha data, projeto e os horários.";
    }
    if ($scope.startTime >= $scope.endTime){
      return "O horário de início não pode ser igual ou posterior ao horário de fim.";
    }
    return "";
  };
  var createLog = function(data){
    WorkLogService.newWorklog(data).then(function(data){
        if(data.status == "201"){
          $('#add-modal').modal('hide');
          $rootScope.$broadcast('log-changed');
        }
      }, function(data){
        alertify.error(data.data.error);
      });
  }
  var updateWorklog = function(data){
    WorkLogService.updateWorklog(data).then(function(data){
        if(data.status == "202"){
          $('#add-modal').modal('hide');
          $rootScope.$broadcast('log-changed');
        }
      }, function(data){
        alertify.error(data.data.error);
      });
  }
  $scope.newWorklog = function(){

    $scope.invalidMessage = validate();
    if($scope.invalidMessage === ""){
      var startDate = dateUtils.setTimeFromAnotherDate($scope.date, $scope.startTime);
      var finishDate = dateUtils.setTimeFromAnotherDate($scope.date, $scope.endTime);
      data = {date: $scope.date, startTime: startDate, endTime: finishDate, observation: $scope.observation, assignment_id : $scope.assignmentSelected, token : UserData.getToken(), logId : $scope.logId};
      if (worklogModalModel.mode === "CREATION"){
          createLog(data);
      }else if (worklogModalModel.mode === "UPDATE"){
          updateWorklog(data);
      }

    }    
  }

  $scope.setDay = function(date){
    $scope.date = date;
    $scope.invalidMessage = "";
  }

  $scope.$on('modal-date-updated', function(event, args) {
    initModalController();
  });


})

// Important to fix a bug with datepicker first unformatted date
.directive('datepickerPopup', function (){
  return {
    restrict: 'EAC',
    require: 'ngModel',
    link: function(scope, element, attr, controller) {
      //remove the default formatter from the input directive to prevent conflict
      controller.$formatters.shift();
    }
  }
});