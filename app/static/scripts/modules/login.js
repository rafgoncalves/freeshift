angular.module('hourBank.login', ['ui.router'])
  .constant('loginConstants',{
    endpoints: {
      login: '/services/login/'
    },
    templates: {
      login: 'views/partials/login.html'
    }
  })
	.config(function($stateProvider, $urlRouterProvider, loginConstants) {
    $stateProvider
      .state('login', {
        url: "/",
        templateUrl: loginConstants.templates.login,
        data: {
          requireLogin: false
        }
      });
  }); 