angular.module('hourBank.users', ['ui.router'])
  .constant('usersConstants',{
    endpoints: {
      users: '/api/users',
      workdays: '/api/user/workdays/', 
      workentries: '/api/workentries/',
      project: '/api/project/<pk>/workdays/',
      company: '/api/company/<pk>/workdays/',
      projectUsers: '/api/project/<pk>/users/',
      companyProjects: '/api/company/<pk>/projects/',
      companyUsers: '/api/company/<pk>/users/',
      manageLog: '/api/workentry/<pk>/manage/',
      createAssignment: '/api/assignments',
      createProject : '/api/projects/',
      createCalendar : '/api/calendars/'
    },
    templates: {
      dashboards: {
        users: 'views/partials/dashboards/main-dashboard.html',
        projects: 'views/partials/dashboards/project-dashboard.html',
        companies: 'views/partials/dashboards/company-dashboard.html'
      },
      creation: {
        project: "views/partials/creation/create-project.html",
        calendar: "views/partials/creation/create-calendar.html",
        assignment: "views/partials/creation/create-assignment.html"
      }
    },
    urls: {
      base: "/user",
      dashboards: {
        mainDashboard: "/main-dashboard.html",
        projectDashboard: "/project-dashboard.html",
        companyDashboard: "/company-dashboard.html"
      },
      creation: {
        project:    "/create-project.html",
        calendar:   "/create-calendar.html",
        assignment: "/create-assignment.html"
      }
    }
  })
	.config(function($stateProvider, $urlRouterProvider, usersConstants) {
  //
  // Now set up the states
  $stateProvider
    .state('user', {
      name: 'user',
    	abstract: true,
    	url: usersConstants.urls.base,
    	template: "<div ui-view></div>",
      data: {
        requireLogin: true
      }
    })
    .state('user.dashboard', {
      name: 'user.dashboard',
    	url: usersConstants.urls.dashboards.mainDashboard,
      templateUrl: usersConstants.templates.dashboards.users,
      requireLogin: true
    })
    .state('user.checkProject', {
      name: 'user.checkProject',
      url: usersConstants.urls.dashboards.projectDashboard,
      templateUrl: usersConstants.templates.dashboards.projects,
      requireLogin: true
    })
    .state('user.checkCompany', {
      name: 'user.checkCompany',
      url: usersConstants.urls.dashboards.companyDashboard,
      templateUrl: usersConstants.templates.dashboards.companies,
      requireLogin: true
    })
    .state('user.createProject', {
      name: 'user.createProject',
      url: usersConstants.urls.creation.project,
      templateUrl: usersConstants.templates.creation.project,
      requireLogin: true
    }) 
    .state('user.createAssignment', {
      name: 'user.createAssignment',
      url: usersConstants.urls.creation.assignment,
      templateUrl: usersConstants.templates.creation.assignment,
      requireLogin: true
    }) 
    .state('user.createCalendar', {
      name: 'user.createCalendar',
      url: usersConstants.urls.creation.calendar,
      templateUrl: usersConstants.templates.creation.calendar,
      requireLogin: true
    });
}); 