var hourBankApp = angular.module('hourBank');

hourBankApp.directive('logManagementModal', ['APPCONFIG', function(APPCONFIG) {
  return {
  	restrict: 'E',
    templateUrl: APPCONFIG.templates.logManagementModal
  };
}]);