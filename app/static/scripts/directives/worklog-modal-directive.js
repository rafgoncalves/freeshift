var hourBankApp = angular.module('hourBank');

hourBankApp.directive('workLogModal', ['APPCONFIG', function(APPCONFIG) {
  return {
  	restrict: 'E',
    templateUrl: APPCONFIG.templates.newWorklogModal
  };
}]);