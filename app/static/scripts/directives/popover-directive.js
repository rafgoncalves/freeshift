var hourBankApp = angular.module('hourBank');

hourBankApp.directive('popoverDirective', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
        	if(attrs.toggle==="popover"){
	        	$(element).popover();
        	}
        }
    };
});