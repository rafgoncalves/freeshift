var hourBankApp = angular.module('hourBank');

hourBankApp.directive('workLogRow', function() {
  return {
  	restrict: 'E',
    templateUrl: '../views/components/worklog-row.html'
  };
});