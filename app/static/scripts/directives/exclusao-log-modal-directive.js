var hourBankApp = angular.module('hourBank');

hourBankApp.directive('excluiLogModal', ['APPCONFIG', function(APPCONFIG) {
  return {
  	restrict: 'E',
    templateUrl: APPCONFIG.templates.deleteWorkEntryModal
  };
}]);