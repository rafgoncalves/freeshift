angular.module('hourBank.users')
  .service('worklogModalModel', function($rootScope){
  	this.date = new Date();
  	this.startTime = new Date();
  	this.endTime = new Date();
  	this.observation = "";
    this.mode="";
    this.id="";
  	this.setDate = function(date){
  		if(typeof(date) === "string"){
  			//Assuming dd-MM-yyyy
  			this.date = new Date(date.split("-")[0], date.split("-")[1]-1, date.split("-")[2]);
  		} else {
	  		this.date = date;
	  	}

  		$rootScope.$broadcast('modal-date-updated');
  	}
});