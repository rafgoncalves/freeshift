var hourBankApp = angular.module('hourBank', 
  ['ui.router', 
  'ui.bootstrap', 
  'hourBank.users',
  'hourBank.login',
  'hourBank.cache',
  'angular-jwt'
  ]);

hourBankApp.constant('APPCONFIG',{
    templates: {
      newWorklogModal:    'views/components/worklog-modal.html',
      newAssignmentModal: 'views/components/assignment-modal.html',
      deleteWorkEntryModal: 'views/components/delete-worklog-modal.html',
      logManagementModal: 'views/components/manage-log-modal.html'
    }
});

hourBankApp.run(function ($rootScope, $state, UserData) {

  $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
    var requireLogin = toState.data.requireLogin;
    if (requireLogin && !UserData.getToken()) {
      $state.current.name = "login";
			$state.transitionTo("login");
			event.preventDefault();
		}else if($state.current.name != toState.name){
         $state.current.name = toState.name;
			   $state.go(toState.name);
    }
	}
);

});

hourBankApp.config(['$stateProvider', '$urlRouterProvider','$tooltipProvider', function($stateProvider, $urlRouterProvider, $tooltipProvider) {
  $tooltipProvider.options({ popoverMode: 'single'});
  //
  // For any unmatched url, redirect to /
  $urlRouterProvider.otherwise("/");

  $("[data-toggle=popover]").popover();
}]); 

