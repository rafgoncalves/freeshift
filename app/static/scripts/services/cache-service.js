angular.module('hourBank.cache', ['ui.router', 'ng', 'ngStorage']).service('UserData', function($localStorage) {
    return {
    	getUsername: function(){
    		return $localStorage.username
    	},
    	setUsername: function(u){
    		$localStorage.username = u;
    	},
    	getAssignments: function(){
    		return $localStorage.assignments;
    	},
    	setAssignments: function(a){
    		$localStorage.assignments = a;
    	},
    	getToken: function(){
    		return $localStorage.token;
    	},
    	setToken: function(t){
    		$localStorage.token = t;
    	},
    	getCompanies: function(){
    		return $localStorage.companies;
    	},
    	setCompanies: function(c){
    		$localStorage.companies = c;
    	},
        addWorkentry: function(w){
            $localStorage.workentries.push(w);
        },
        getWorkentries: function(){
            return $localStorage.workentries;
        },
        addAllWorkentries: function(entries){
            for(var i=0; i < entries.length; i++){
                $localStorage.workentries.push(entries[i]);
            }
        },        
        removeAllWorkentries: function(){
            $localStorage.workentries = [];
        },
        getWorkentryById: function(id){
            var i = 0;
            while ((i < $localStorage.workentries.length) && ($localStorage.workentries[i].id != id)){
                i++;
            }
            if (i < $localStorage.workentries.length){
                return $localStorage.workentries[i];
            }
            return 'undefined';
        },
        clear: function(){
            $localStorage.username = "";
            $localStorage.companies = "";
            $localStorage.token = "";
            $localStorage.assignments = "";
            $localStorage.workentries = [];
        }
    };
});
