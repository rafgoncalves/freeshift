angular.module('hourBank.users').service('projectDaysMiddleware',  function(stringUtils){
	return {
		splitEntries : function(days){
			var entries = [];
			for ( i = 0; i < days.length; ++i){
				var userData = [];
				var date = stringUtils.convertDateToString(days[i].date);
	            var map = {};
	            for ( j = 0; j < days[i].work_entries.length; ++j){
	            	if(!(map[days[i].work_entries[j].user])){
						map[days[i].work_entries[j].user] = [];
	            	}
	            	map[days[i].work_entries[j].user].push(days[i].work_entries[j]);
	            }
	            for(key in map){
					userData.push({'work_logs' : map[key], 'nome' : key});
	            }
				entries.push({'userData' : userData, 'date' : date});
	        }
			return entries;
		}
    };
});