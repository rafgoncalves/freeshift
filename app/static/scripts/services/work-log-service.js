angular.module('hourBank.users').service('WorkLogService', ['$http', 'usersConstants', function($http, usersConstants){
    return {
      newWorklog : function(workLog){
        var promise = $http({
          url: usersConstants.endpoints.workentries,
          method: 'POST',
          headers: {
            'Authorization': workLog.token
          },
          data: {'date' : workLog.date, 'startTime' : workLog.startTime, 'endTime' : workLog.endTime, 
                 'observation' : workLog.observation, 'assignment_id' : workLog.assignment_id}
        }).success(function (data) {
          return data;
        }).error(function (data) {
          return data;
        });
        return promise;
      },
      updateWorklog : function(workLog){
        var promise = $http({
          url: usersConstants.endpoints.workentries + workLog.logId + '/',
          method: 'PUT',
          headers: {
            'Authorization': workLog.token
          },
          data: {'date' : workLog.date, 'startTime' : workLog.startTime, 'endTime' : workLog.endTime, 
                 'observation' : workLog.observation, 'assignment_id' : workLog.assignment_id}
        }).success(function (data) {
          return data;
        }).error(function (data) {
          return data;
        });
        return promise;
      },
      deleteWorklog : function(workLogId, token){
          var promise = $http({
            url: usersConstants.endpoints.workentries + workLogId + '/',
            method: 'DELETE',
            headers: {
              'Authorization': token
            }            
          }).success(function (data) {
            return data;
          }).error(function (data) {
            return data;
          });
          return promise;
      },
      manageWorklog : function(workLogId, approval, token){
          var promise = $http({
            url: usersConstants.endpoints.manageLog.replace('<pk>', workLogId),
            method: 'PUT',
            headers: {
              'Authorization': token
            },
            data: {'approved' : approval}
          }).success(function (data) {
            return data;
          }).error(function (data) {
            return data;
          });
          return promise;
      }
    };
}]);