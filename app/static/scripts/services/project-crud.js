angular.module('hourBank.users').service('ProjectCRUDService', ['usersConstants','$http', function( usersConstants, $http){
      return {
        createProject : function(project, token){
          var promise = $http({
            url: usersConstants.endpoints.createProject,
            method: 'POST',
            headers: {
              'Authorization': token
            },
            data: {'color' : project.color, 'name' : project.name,'company_id' : project.company_id}
          }).success(function (data) {
            return data;
          }).error(function (data) {
            return data;
          });
          return promise;
        }
      };
}]);