angular.module('hourBank.users').service('hoursIntervalService',  function(stringUtils){
	return {
		normalizeInterval : function(days){
			for ( i = 0; i < days.length; ++i){
	            days[i].date = new Date(days[i].date);
	            for ( j = 0; j < days[i].work_entries.length; ++j){
	              days[i].work_entries[j].start = new Date(days[i].work_entries[j].start);
	              days[i].work_entries[j].start = days[i].work_entries[j].start.getHours() + ":" + days[i].work_entries[j].start.getMinutes();
	              days[i].work_entries[j].finish = new Date(days[i].work_entries[j].finish);
	              days[i].work_entries[j].finish = days[i].work_entries[j].finish.getHours() + ":" + days[i].work_entries[j].finish.getMinutes();
	            }
         	}
         	return days;
		},
		getHoursInterval : function(days){		
			minStartHour = "24:00"
			maxFinishHour = "00:00"
			for (i = 0; i < days.length; ++i){
				for(j = 0; j < days[i].work_entries.length; j++){

					if (stringUtils.convertTimeStringToFloat(minStartHour) > stringUtils.convertTimeStringToFloat(days[i].work_entries[j].start)) {
						minStartHour = days[i].work_entries[j].start;
					}

					if (stringUtils.convertTimeStringToFloat(maxFinishHour) < stringUtils.convertTimeStringToFloat(days[i].work_entries[j].finish)) {
						maxFinishHour = days[i].work_entries[j].finish;
					}
				}
			}
			startHour =  parseInt(minStartHour.split(":")[0]);
			endHour =  parseInt(maxFinishHour.split(":")[0]);
			if (startHour === 24 && endHour === 0){
				startHour = 7;
				endHour = 17;	
			}

			if (parseInt(maxFinishHour.split(":")[1]) > 0){
				endHour++;
			}
			hourWidth = 100/(endHour - startHour);

			var headerHourSpacing = 97/(endHour - startHour);

			hours = [];
			for ( i = startHour; i <= endHour; i++){
				hours.push({'hour': i + ":00", 'position': (i-startHour)*headerHourSpacing + 1.5 + '%'});

			}
			startHour = startHour.toString() + ":00";
			endHour = endHour.toString() + ":00";
			var interval = {'minStart' : startHour, 'maxEnd' : endHour, 'hourWidth' : hourWidth, 'hours' : hours};
			return interval;
		}
    };
});