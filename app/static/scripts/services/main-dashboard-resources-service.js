angular.module('hourBank.users').service('MainDashboardResourceService', ['usersConstants','$http', function( usersConstants, $http){
  return {
        getWorkDays: function(query){
          var promise = $http({
            url: usersConstants.endpoints.workdays,
            method: 'GET',
        		headers: {
        			'Authorization': query.token
        		},
            params: {
            	start_date: query.start_date,
            	end_date: query.end_date
            }
          }).success(function (data) {
            return data;
          }).error(function (data) {
            return data;
          });
          return promise;
        }
      };
}]);