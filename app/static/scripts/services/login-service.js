angular.module('hourBank.login').service('loginService', [ 'loginConstants', function(loginConstants){
    return {
        autenticate : function ($http, username, password) {
            var promise = $http({
                url: loginConstants.endpoints.login,
                method: "POST",
                headers: { 'Content-Type': 'application/json' },
                data: {'username':username, 'password':password}
                }).success(function(data){
                    return data;
                }).error(function(data){
                    return data;
                });
            return promise;
        }
    };
}]);
