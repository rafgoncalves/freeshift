angular.module('hourBank.users').service('dateUtils',  function(){
	return {
		addDays : function(oldDate, daysAdded){
			var newDate = new Date(oldDate);
            newDate = new Date(newDate.setDate(newDate.getDate() + daysAdded));
            return newDate;
		},
		subtractDays : function(oldDate, daysAdded){
			var newDate = this.addDays(oldDate, daysAdded * -1);
            return newDate;
		},
		setTimeFromAnotherDate : function(date, time){
			var newDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), time.getHours(), time.getMinutes(), time.getSeconds(), time.getMilliseconds());
			return newDate;
		},
		convertHourToDate : function(t){
			var data = new Date();
			var hora = parseInt(t.split(':')[0]);
			var minuto = parseInt(t.split(':')[1]);
			data.setHours(hora);
			data.setMinutes(minuto);
			return data;
		}
    };
});