angular.module('hourBank.users').service('popoverManager',  function(){
	return {
		hidePopover : function(allPopovers){
			if(allPopovers.length === 1){
				var s = '#' + allPopovers[0].id;
				$(s).popover('hide');
			}
			return "";
		},
		hideExceedingPopover : function(openedPopover, allPopovers){
			if (allPopovers.length > 1){
				var s = '#';
				if(allPopovers[0].id === openedPopover){
					s = s + allPopovers[0].id;
					openedPopover = allPopovers[1].id;
				}else{
					s = s + allPopovers[1].id;
					openedPopover = allPopovers[0].id;
				}
				$(s).popover('hide');
			}else if(allPopovers.length === 1){
				openedPopover = allPopovers[0].id;
			}
			return openedPopover;
		}
   };
});