angular.module('hourBank.users').service('worklogBlockConstructorService',  function(stringUtils){
	return {		
		getTimeBlocks : function(workEntries, startHour, endHour, hourWidth){
			currentTime = stringUtils.convertTimeStringToFloat(startHour);
			var blocks = [];
			for (var i = 0; i < workEntries.length; ++i){
				if (currentTime < stringUtils.convertTimeStringToFloat(workEntries[i].start)) {
					blocks.push({blockWidth: (stringUtils.convertTimeStringToFloat(workEntries[i].start)-currentTime)*hourWidth, start: currentTime.toString(), finish: workEntries[i].start, color: "transparent"});
				}
				blocks.push({id: workEntries[i].id, blockWidth: (stringUtils.convertTimeStringToFloat(workEntries[i].finish)-stringUtils.convertTimeStringToFloat(workEntries[i].start))*hourWidth, start: stringUtils.convertTimeStringToFloat(workEntries[i].start), finish:currentTime, color: workEntries[i].project_color});
				currentTime = stringUtils.convertTimeStringToFloat(workEntries[i].finish);
				
				if (currentTime < endHour){
					blocks.push({blockWidth: (endHour-currentTime)*hourWidth, color: "transparent"});
				}
			}
			return blocks;
		}

    };
});