angular.module('hourBank.users').service('companyDaysMiddleware',  function(stringUtils){
	return {
		splitEntries : function(days){
			var entries = [];
			for ( i = 0; i < days.length; ++i){
	            var map = {};
				var projectData = [];
				var date = stringUtils.convertDateToString(days[i].date);
				for ( j = 0; j < days[i].work_entries.length; ++j){
	            	if(!(map[days[i].work_entries[j].project])){
						map[days[i].work_entries[j].project] = {};
	            	}
	            	if (!map[days[i].work_entries[j].project][days[i].work_entries[j].user]){
	            		map[days[i].work_entries[j].project][days[i].work_entries[j].user] = [];
	            	}
	            	map[days[i].work_entries[j].project][days[i].work_entries[j].user].push(days[i].work_entries[j]);
	            }
	            for(var key in map){
					var userData = [];
					for(var innerKey in map[key]){
						userData.push({'nome' : innerKey, 'work_entries' : map[key][innerKey]});
					}
					projectData.push({'projeto' : key, 'userData' : userData} );
	            }
				entries.push({'projectData' : projectData, 'date' : date});	            
	        }
			return entries;
		}
    };
});