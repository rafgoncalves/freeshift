angular.module('hourBank.users').service('shareService',  function($localStorage){
	return {
    	getAssignmentSelected: function(){
    		return $localStorage.assignmentSelected;
    	},
    	setAssignmentSelected: function(a){
    		$localStorage.assignmentSelected = a;
    	},
    	getCompanySelected: function(){
    		return $localStorage.companySelected;
    	},
    	setCompanySelected: function(c){
    		$localStorage.companySelected = c;
    	}
    };
});