angular.module('hourBank.users').service('CalendarCRUDService', ['usersConstants','$http', function( usersConstants, $http){
      return {
        createCalendar : function(calendar, token){
          var promise = $http({
            url: usersConstants.endpoints.createCalendar,
            method: 'POST',
            headers: {
              'Authorization': token
            },
            data: {'name' : calendar.name,'company_id' : calendar.company_id, 'obs' : calendar.observation,
            'default_monday' : calendar.horas.segunda, 'default_tuesday' : calendar.horas.terca,
            'default_wednesday' : calendar.horas.quarta, 'default_thursday' : calendar.horas.quinta,
            'default_friday' : calendar.horas.sexta,  'default_saturday' : calendar.horas.sabado,
            'default_sunday' : calendar.horas.domingo}
          }).success(function (data) {
            return data;
          }).error(function (data) {
            return data;
          });
          return promise;
        }
      };
}]);