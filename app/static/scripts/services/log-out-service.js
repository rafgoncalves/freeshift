angular.module('hourBank.users').service('logOutService',  function(UserData, $state){
	return {
		logOut : function(){
			UserData.clear();
        	$state.go("login");
		}
    };
});