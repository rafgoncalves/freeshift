angular.module('hourBank.users').service('AssignmentCRUDService', ['$http', 'usersConstants', function($http, usersConstants){
    return {
      newAssignment : function(newAssignmentValues, token){
        var promise = $http({
          url: usersConstants.endpoints.createAssignment,
          method: 'POST',
          headers: {
            'Authorization': token
          },
          data: {'role' : newAssignmentValues.role, 'project' : newAssignmentValues.project, 'company' : newAssignmentValues.company, 'user' : newAssignmentValues.user}
        }).success(function (data) {
          return data;
        }).error(function (data) {
          return data;
        });
        return promise;
      }
    };
  }]);