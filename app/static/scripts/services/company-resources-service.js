angular.module('hourBank.users').service('CompanyResourcesService', ['usersConstants','$http', function( usersConstants, $http){
      return {
        getWorkDays: function(query){
          var promise = $http({
            url: usersConstants.endpoints.company.replace('<pk>', query.companyId),
            method: 'GET',
        		headers: {
        			'Authorization': query.token
        		},
            params: {
            	start_date: query.start_date,
            	end_date: query.end_date,
              user_id: query.userId,
              project_id : query.projectId,
              waiting_approval : query.waiting_approval
            }
          }).success(function (data) {
            return data;
          }).error(function (data) {
            return data;
          });
          return promise;
        },
        getCompanyWorkers: function(query){
          var promise = $http({
            url: usersConstants.endpoints.companyUsers.replace('<pk>', query.companyId),
            method: 'GET',
            headers: {
              'Authorization': query.token
            }
          }).success(function (data) {
            return data;
          }).error(function (data) {
            return data;
          });
          return promise;          
        },
        getCompanyProjects: function(query){
          var promise = $http({
            url: usersConstants.endpoints.companyProjects.replace('<pk>', query.companyId),
            method: 'GET',
            headers: {
              'Authorization': query.token
            }
          }).success(function (data) {
            return data;
          }).error(function (data) {
            return data;
          });
          return promise;          
        }
      };
}]);