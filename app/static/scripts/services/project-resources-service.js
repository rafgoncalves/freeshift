angular.module('hourBank.users').service('ProjectResourcesService', ['usersConstants','$http', function( usersConstants, $http){
      return {
        getWorkDays: function(query){
          var promise = $http({
            url: usersConstants.endpoints.project.replace('<pk>', query.projectId),
            method: 'GET',
        		headers: {
        			'Authorization': query.token
        		},
            params: {
            	start_date: query.start_date,
            	end_date: query.end_date,
              user_id: query.userId,
              waiting_approval: query.waiting_approval
            }
          }).success(function (data) {
            return data;
          }).error(function (data) {
            return data;
          });
          return promise;
        },
        getProjectWorkers: function(query){
          var promise = $http({
            url: usersConstants.endpoints.projectUsers.replace('<pk>', query.projectId),
            method: 'GET',
            headers: {
              'Authorization': query.token
            }
          }).success(function (data) {
            return data;
          }).error(function (data) {
            return data;
          });
          return promise;          
        }
      };
}]);