angular.module('hourBank.users').service('stringUtils',  function(){
	return {
		convertDateToString : function(d){
            var date = d.toISOString();
            date = date.slice(0,date.indexOf("T"));
            return date;
		},
		convertTimeStringToFloat : function(timeString) {
			return parseFloat(timeString.split(':')[0])+parseFloat(timeString.split(':')[1])/60.0;
		},
		mask : function(time){
			if (time.length === 1){
				return "0" + time;
			}
			return time;
		},

		sliceHour : function(hour){
			hour = hour.slice(0,hour.indexOf(':'));
			hour = this.mask(hour);
			return hour;
		},
		sliceMinute : function(minute){
			minute = minute.slice(1 + minute.indexOf(':'));
			minute = this.mask(minute);
			return minute;
		}
    };
});