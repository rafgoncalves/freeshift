from flask import Flask
from app.services import mod_services
from app.api import mod_api
import config


app = Flask(__name__,
            static_url_path=config.STATIC_URL_PATH,
            static_folder=config.STATIC_FOLDER)

app.config.from_object(config)
app.register_blueprint(mod_services)
app.register_blueprint(mod_api)