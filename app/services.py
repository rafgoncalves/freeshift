from flask import Blueprint, request, jsonify, abort
from app.models import User, Contract, Calendar, Company, Project, Assignment
from config import SECRET_KEY
from peewee import JOIN

import jwt

mod_services = Blueprint('services', __name__, url_prefix='/services')


@mod_services.route('/login/', methods=['POST'])
def login():
    username = request.json.get('username')
    password = request.json.get('password')

    if username is None or password is None:
        # BAD REQUEST
        abort(400)

    try:
        user = User.get(User.login == username, User.password == password)

    except User.DoesNotExist:
        # UNAUTHORIZED
        abort(401)

    assignments = (User
                   .select(Assignment.id.alias('assignment_id'), Company.name.alias('company_name'),
                           Project.name.alias('project_name'), Project.id.alias('project_id'),
                           Assignment.project_role.alias('project_role'))
                   .join(Contract, JOIN.LEFT_OUTER)
                   .join(Calendar, JOIN.LEFT_OUTER)
                   .join(Company, JOIN.LEFT_OUTER)
                   .join(Assignment, JOIN.LEFT_OUTER, on=(Assignment.contract == Contract.id))
                   .join(Project, JOIN.LEFT_OUTER)
                   .where(User.id == user.id)
                   .aggregate_rows().dicts()
    )

    companies = (User
                 .select(Company.name.alias('name'), Contract.contract_type.alias('role'), Company.id.alias('id'))
                 .join(Contract, JOIN.LEFT_OUTER)
                 .join(Calendar, JOIN.LEFT_OUTER)
                 .join(Company, JOIN.LEFT_OUTER)
                 .where(User.id == user.id)
                 .aggregate_rows().dicts()
    )

    payload = {'name': user.name, 'login': user.login, 'assignments': list(assignments), 'companies': list(companies)}

    token = jwt.encode(payload, key=SECRET_KEY)
    return jsonify({'token': token.decode(encoding='ASCII')})
