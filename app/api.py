from flask import Blueprint, abort, request
from restless.fl import FlaskResource
from restless.preparers import FieldsPreparer
from playhouse.shortcuts import model_to_dict
from peewee import prefetch, JOIN
from config import SECRET_KEY, DATABASE
from datetime import datetime
from restless.exceptions import BadRequest, Unauthorized, NotFound
import jwt
from functools import wraps

from app.models import User, Company, Project, Calendar, CustomDay, WorkEntry, Assignment, Contract

mod_api = Blueprint('api', __name__, url_prefix='/api')


class FreeShiftResource(FlaskResource):
    access_type = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = None
        self.payload = None

    def user_has_access_company(self, pk):
        for comp in self.payload.get('companies'):
            if comp.get('id') == int(pk) and comp.get('role') != 'WORKER':
                return True
        return False

    def user_has_access_assignment(self, pk):
        for assignment in self.payload.get('assignments'):
            if assignment.get('project_id') == int(pk) and assignment.get('project_role') != 'WORKER':
                return True
        return False

    def user_is_himself(self, pk):
        return self.user.id == int(pk)

    def is_authenticated(self):
        self.get_user_from_token()
        if 'pk' in self.request.view_args:
            if callable(self.access_type):
                return self.access_type(self.request.view_args.get('pk'))
        return True

    def get_user_from_token(self):
        # TODO WRITE DOCSTRING DESCRIBING THE BEHAVIOR
        try:
            token = request.headers.get('Authorization')
        except AttributeError:
            raise Unauthorized()

        try:
            self.payload = jwt.decode(token.encode(encoding='ASCII'), key=SECRET_KEY)
        except jwt.DecodeError:
            raise Unauthorized()

        try:
            self.user = User.get(User.login == self.payload.get('login'))
        except User.DoesNotExist:
            raise Unauthorized()

    def validate_data(self, required_fields):
        for f in required_fields:
            if f not in self.data or self.data[f] is None:
                raise BadRequest(f)

    @staticmethod
    def access_validation(method):

        def decorate(f):
            @wraps(f)
            def wrapper(*args, **kwargs):
                self = args[0]
                if method(self, self.request.view_args.get('pk')):
                    return f(*args, **kwargs)
                else:
                    raise Unauthorized()

            return wrapper
        return decorate


class UserResource(FreeShiftResource):
    def list(self):
        # TODO WRITE DOCSTRING DESCRIBING THE RETURNED DATA
        users = User.select()
        return [model_to_dict(user) for user in users]

    def create(self):
        self.validate_data(['name', 'login', 'password', 'email'])
        if User.select().where(User.login == self.data['login']).exists():
            raise BadRequest('User with this data already exists')
        new_user = User.create(name=self.data['name'],
                               login=self.data['login'],
                               password=self.data['password'],
                               email=self.data['email'])
        return model_to_dict(new_user)

    @FreeShiftResource.access_validation(FreeShiftResource.user_is_himself)
    def detail(self, pk):
        try:
            result = User.get(User.id == pk)
        except User.DoesNotExist:
            raise NotFound()
        return model_to_dict(result)

    @FreeShiftResource.access_validation(FreeShiftResource.user_is_himself)
    def delete(self, pk):
        try:
            result = User.get(User.id == pk)
        except User.DoesNotExist:
            raise BadRequest()
        result.delete_instance()

    @FreeShiftResource.access_validation(FreeShiftResource.user_is_himself)
    def update(self, pk):
        try:
            result = User.get(User.id == pk)
        except User.DoesNotExist:
            raise BadRequest()

        if 'name' in self.data and self.data['name'] is not None:
            result.name = self.data['name']

        if 'password' in self.data and self.data['password'] is not None:
            result.password = self.data['password']

        if 'email' in self.data and self.data['email'] is not None:
            result.email = self.data['email']

        return model_to_dict(result.save())


UserResource.add_url_rules(mod_api, rule_prefix='/users/')


class UserProjectsResource(FreeShiftResource):
    @FreeShiftResource.access_validation(FreeShiftResource.user_is_himself)
    def list(self, pk):
        self.get_user_from_token()
        if self.user.id != pk:
            raise Unauthorized()
        projects = (Assignment.select(Project.name,
                                      Company.name.alias('company_name'),
                                      Assignment.project_role.alias('role'))
                    .join(Project)
                    .join(Company).join(Contract, JOIN.LEFT_OUTER,
                                        on=(Assignment.contract == Contract.id))
                    .join(User, JOIN.LEFT_OUTER)
                    .where(User.id == pk)
                    .aggregate_rows().dicts())
        return projects

mod_api.add_url_rule('/user/<pk>/projects/',
                     endpoint='api_user_projects_list',
                     view_func=UserProjectsResource.as_list(),
                     methods=['GET'])


class UserAssignmentsResource(FreeShiftResource):
    @FreeShiftResource.access_validation(FreeShiftResource.user_is_himself)
    def list(self, pk):
        projects = (Assignment.select(Assignment.id, Project.id.alias('project_id'),
                                      Project.name.alias('project_name'),
                                      Company.name.alias('company_name'),
                                      Assignment.project_role.alias('role'))
                    .join(Project)
                    .join(Company).join(Contract, JOIN.LEFT_OUTER,
                                        on=(Assignment.contract == Contract.id))
                    .join(User, JOIN.LEFT_OUTER)
                    .where(User.id == pk)
                    .aggregate_rows().dicts())
        return projects

mod_api.add_url_rule('/user/<pk>/assignments/',
                     endpoint='api_user_assignments_list',
                     view_func=UserAssignmentsResource.as_list(),
                     methods=['GET'])


class UserCompaniesResource(FreeShiftResource):
    @FreeShiftResource.access_validation(FreeShiftResource.user_is_himself)
    def list(self, pk):
        companies = (Contract.select(Company.name, Company.id.alias('company_id'), Contract.contract_type)
                     .join(Calendar,
                           on=(Contract.calendar == Calendar.id))
                     .join(Company, JOIN.LEFT_OUTER,
                           on=(Calendar.company == Company.id))
                     .join(User, JOIN.LEFT_OUTER,
                           on=(Contract.user == User.id))
                     .where(User.id == pk)
                     .aggregate_rows().dicts())
        return companies

mod_api.add_url_rule('/user/<pk>/companies/',
                     endpoint='api_user_companies_list',
                     view_func=UserCompaniesResource.as_list(),
                     methods=['GET'])


class WorkDayResource(FreeShiftResource):
    _base_query = (WorkEntry.select(WorkEntry.id,
                                    WorkEntry.created_at,
                                    WorkEntry.start, WorkEntry.finish,
                                    User.name.alias('user'),
                                    Project.name.alias('project'),
                                    Project.color.alias('project_color'),
                                    Assignment.id.alias('assignment_id'),
                                    WorkEntry.obs,
                                    WorkEntry.status)
                   .join(Assignment, JOIN.LEFT_OUTER)
                   .join(Project, JOIN.LEFT_OUTER)
                   .join(Contract, JOIN.LEFT_OUTER,
                         on=Assignment.contract == Contract.id)
                   .join(User, JOIN.LEFT_OUTER))

    def get_work_days(self, user_id, start_date, end_date):
        return (self._base_query.where((Contract.user == user_id) &
                                       (WorkEntry.start >= start_date) &
                                       (WorkEntry.start <= end_date))
                .order_by(WorkEntry.start.asc()).aggregate_rows().dicts())

    def list(self):
        """Returns a collection of workentries, grouped by date.

        :return:[{'date': '2015-04-%d' % d,
        #            'work_entries': [{'id': i, 'created_at': '2015-04-%d %d:08:24.067891' % (d, h),
        #                              'start': '2015-04-%d %d:08:24.067891' % (d, h),
        #                              'finish': '2015-04-%d %d:08:24.067891' % (d, h + 1), 'user': self.user.name,
        #                              'project': 'Square AT&T',
        #                              'assignment_id': 1, 'obs': 'obs'} for h in hours]}
        #           for i, d in days.items()]
        """

        start_date = request.args.get('start_date')
        end_date = request.args.get('end_date')
        if end_date is None:
            raise BadRequest()

        if start_date is None:
            raise BadRequest()

        self.get_user_from_token()

        result = self.get_work_days(self.user.id, start_date, end_date)

        return_value = []
        last_date = None
        for item in result:
            if item['start'].date() != last_date:
                last_date = item['start'].date()
                return_value.append({'date': last_date, 'work_entries': []})

            return_value[-1]['work_entries'].append(item)

        return return_value

    def detail(self, *args, **kwargs):
        date = request.args.get('date')
        if date is None:
            raise BadRequest()

        result = self.get_work_days(self.user.id, date, date)

        return {'date': result['start'].date(), 'work_entries': [result]}

WorkDayResource.add_url_rules(mod_api, rule_prefix='/user/workdays/')


class ContractResource(FreeShiftResource):
    def create(self, *args, **kwargs):
        self.validate_data(('user_id', 'calendar_id', 'contract_type'))

        try:
            calendar = Calendar.get(Calendar.id == self.data['calendar_id'])
        except Calendar.DoesNotExist:
            raise BadRequest()
        # Validate if user is company admin|coordinator
        if not self.user_has_access_company(calendar.company.id):
            raise Unauthorized()

        if (Contract.select().where(Contract.user == self.data['user_id'],
                                    Contract.calendar == self.data['calendar_id'])
                    .exists()):
            raise BadRequest('Contract already exists')

        return model_to_dict(Contract.create(calendar=self.data['calendar_id'],
                                             user=self.data['user_id'],
                                             contract_type=self.data['contract_type']))

    @FreeShiftResource.access_validation(FreeShiftResource.user_has_access_company)
    def update(self, pk):
        self.validate_data(('user_id', 'calendar_id', 'contract_type'))
        try:
            contract = Contract.get(Contract.id == pk)
        except Contract.DoesNotExist:
            raise NotFound()
        contract.user = self.data['user_id']
        contract.calendar = self.data['calendar_id']
        contract.contract_type = self.data['contract_type']
        contract.save()
        return model_to_dict(contract)

    @FreeShiftResource.access_validation(FreeShiftResource.user_has_access_company)
    def detail(self, pk):
        try:
            contract = Contract.get(Contract.id == pk)
        except Contract.DoesNotExist:
            raise NotFound()
        return model_to_dict(contract)

    @FreeShiftResource.access_validation(FreeShiftResource.user_has_access_company)
    def delete(self, pk):
        try:
            contract = Contract.get(Contract.id == pk)
        except Contract.DoesNotExist:
            raise NotFound()
        contract.delete_instance()

ContractResource.add_url_rules(mod_api, rule_prefix='/contracts/')


class CalendarResource(FreeShiftResource):
    @FreeShiftResource.access_validation(FreeShiftResource.user_has_access_company)
    def list(self):
        # TODO WRITE DOCSTRING DESCRIBING THE RETURNED DATA
        calendars = Calendar.select()
        custom_days = CustomDay.select()

        full_calendars = prefetch(calendars, custom_days)

        return [model_to_dict(c,
                              backrefs=True,
                              recurse=False,
                              exclude=[Calendar.contract_set])
                for c in full_calendars]

    @FreeShiftResource.access_validation(FreeShiftResource.user_has_access_company)
    def detail(self, pk):
        # TODO WRITE DOCSTRING DESCRIBING THE RETURNED DATA
        calendar = Calendar.select().where(Calendar.id == pk)
        custom_days = CustomDay.select()

        try:
            full_calendar = prefetch(calendar, custom_days).get()
            return model_to_dict(full_calendar,
                                 backrefs=True,
                                 recurse=False)

        except Calendar.DoesNotExist:
            raise NotFound()

    def create(self):
        self.validate_data(('company_id', 'name', 'obs',
                            'default_monday', 'default_tuesday',
                            'default_wednesday', 'default_thursday',
                            'default_friday',  'default_saturday',
                            'default_sunday'))

        # Validate if user is company admin|coordinator
        if not self.user_has_access_company(int(self.data['company_id'])):
            raise Unauthorized()

        if (Calendar.select().where(Calendar.name == self.data['name'],
                                    Calendar.company == self.data['company_id']).exists()):
            raise BadRequest('Calendar with this name already exists')

        calendar = Calendar.create(company=self.data['company_id'], name=self.data['company_id'],
                                   obs=self.data['obs'], default_monday=self.data['default_monday'],
                                   default_tuesday=self.data['default_tuesday'],
                                   default_wednesday=self.data['default_wednesday'],
                                   default_thursday=self.data['default_thursday'],
                                   default_friday=self.data['default_friday'],
                                   default_saturday=self.data['default_saturday'],
                                   default_sunday=self.data['default_sunday'])

        return model_to_dict(calendar)

CalendarResource.add_url_rules(mod_api, rule_prefix='/calendars/')


class AssignmentResource(FreeShiftResource):
    def create(self):
        self.validate_data(['contract_id', 'project_id', 'project_role'])

        try:
            contract = (Contract.select().join(Calendar)
                        .where(Contract.id == self.data['contract_id']).get())
        except Contract.DoesNotExist:
            raise BadRequest()

        # Validate if user is company admin|coordinator
        if not self.user_has_access_company(contract.calendar.company.id):
            raise Unauthorized()

        # Validate if user has access to the project
        if not self.user_has_access_assignment(self.data['project_id']):
            raise Unauthorized()

        #check if user is already assigned to project
        if (Assignment.select()
                    .where(Assignment.contract == self.data['contract_id'],
                           Assignment.project == self.data['project_id'])).exists():
            raise BadRequest('User already assigned to this project')

        try:
            project = Project.get(id=self.data['project_id'])
        except Project.DoesNotExist:
            raise BadRequest()

        color = project.color
        if self.data['color'] is not None:
            color = self.data['color']

        return model_to_dict(Assignment.create(contract=self.data['contract_id'],
                                               project=self.data['project_id'],
                                               project_role=self.data['project_role'],
                                               color = color))

    def detail(self, pk):
        try:
            assignment = Assignment.get(Assignment.id == pk)
        except Assignment.DoesNotExist:
            raise NotFound()
        if not self.user_has_access_assignment(assignment.project):
            raise Unauthorized()
        return model_to_dict(assignment)

    def delete(self, pk):
        try:
            assignment = Assignment.get(Assignment.id == pk)
        except Assignment.DoesNotExist:
            raise NotFound()
        if not self.user_has_access_assignment(assignment.project):
            raise Unauthorized()
        assignment.delete_instance()

    def update(self, pk):
        try:
            assignment = Assignment.get(Assignment.id == pk)
        except Assignment.DoesNotExist:
            raise NotFound()

        self.validate_data(['contract_id', 'project_id', 'project_role'])

        try:
            contract = (Contract.select().join(Calendar)
                        .where(Contract.id == self.data['contract_id']).get())
        except Contract.DoesNotExist:
            raise BadRequest()

        # Validate if user is company admin|coordinator
        if not self.user_has_access_company(contract.calendar.company.id):
            raise Unauthorized()

        # Validate if user has access to the project
        if not self.user_has_access_assignment(self.data['project_id']):
            raise Unauthorized()

        if (Assignment.select()
                    .where(Assignment.id != pk, Assignment.contract == self.data['contract_id'],
                           Assignment.project == self.data['project_id'])).exists():
            raise BadRequest('User already assigned to this project')

        color = assignment.color
        if self.data['color'] is not None:
            color = self.data['color']

        assignment.project = self.data['project_id']
        assignment.contract = self.data['contract_id']
        assignment.project_role = self.data['project_role']
        assignment.color = color
        return model_to_dict(assignment.save())


AssignmentResource.add_url_rules(mod_api, rule_prefix='/assignments/')


class WorkEntryResource(FreeShiftResource):
    _base_query = (WorkEntry.select(WorkEntry.id,
                                    WorkEntry.created_at,
                                    WorkEntry.start, WorkEntry.finish,
                                    User.name.alias('user'),
                                    Project.name.alias('project'),
                                    Project.color.alias('project_color'),
                                    Assignment.id.alias('assignment_id'),
                                    WorkEntry.obs)

                   .join(Assignment, JOIN.LEFT_OUTER)
                   .join(Project, JOIN.LEFT_OUTER)
                   .join(Contract, JOIN.LEFT_OUTER,
                         on=Assignment.contract == Contract.id)

                   .join(User, JOIN.LEFT_OUTER))

    def _base_validator(self, required_fields, update, id):
        self.validate_data(required_fields)
        assignment = (Assignment.select()
                      .join(Contract, JOIN.LEFT_OUTER)
                      .join(User, JOIN.LEFT_OUTER)
                      .where(User.id == self.user.id
                             and Assignment.id == self.data['assignment_id']))

        if not assignment.exists():
            return False

        # CHECK IF THERES NO WORKENTRY FOR THIS USER DURING THIS INTERVAL
        result = WorkEntry.select().join(Assignment).join(Contract).join(User).where(
            (User.id == self.user.id) & (((WorkEntry.start <= self.data['startTime'])
                                          & (WorkEntry.finish >= self.data['startTime'])) |
                                         ((WorkEntry.start <= self.data['endTime']) &
                                          (WorkEntry.finish >= self.data['endTime'])))
        )
        if update:
            result = result.where(WorkEntry.id != id)
        return not result.exists()

    def _validate_create(self, required_fields):
        return self._base_validator(required_fields, False, None)

    def _validate_update(self, required_fields, id):
        return self._base_validator(required_fields, True, id)

    def list(self):
        """ Returns a collection of workentries.
        :return: [{'id': 'workentry id', 'created_at': 'created date', 'start': 'start date',
        #            'finish': 'finish date', 'user': 'username', 'project': 'project name',
        #            'assignment_id': assignment id, 'obs': 'obs'}]
        """
        self.get_user_from_token()
        return (self._base_query.where(Contract.user == self.user.id)
                .aggregate_rows()
                .dicts())

    def update(self, pk):
        if self._validate_update(('assignment_id', 'startTime', 'endTime'), pk):
            try:
                work_entry = (WorkEntry.select().join(Assignment)
                              .join(Contract, JOIN.LEFT_OUTER)
                              .join(User, JOIN.LEFT_OUTER)
                              .where(WorkEntry.id == pk, User.id == self.user.id).get())
            except WorkEntry.DoesNotExist:
                raise NotFound()
            work_entry.start = self.data['startTime']
            work_entry.finish = self.data['endTime']
            work_entry.assignment = self.data['assignment_id']
            work_entry.obs = self.data['observation']
            work_entry.created_at=datetime.now()
            work_entry.save()
        else:
            raise BadRequest()
        return model_to_dict(work_entry)

    def delete(self, pk):
        try:
            work_entry = (WorkEntry.select()
                          .join(Assignment)
                          .join(Contract)
                          .join(User)
                          .where(WorkEntry.id == pk,
                                 User.id == self.user.id).get())
        except WorkEntry.DoesNotExist:
            raise NotFound()
        work_entry.delete_instance()

    def create(self, *args, **kwargs):
        # TODO WRITE DOCSTRING DESCRIBING THE RETURNED DATA
        self.get_user_from_token()

        if self._validate_create(('assignment_id', 'startTime', 'endTime')):

            result = WorkEntry.create(start=self.data['startTime'],
                                    finish=self.data['endTime'],
                                    assignment=self.data['assignment_id'],
                                    obs=self.data['observation'],
                                    created_at=datetime.now())
            return model_to_dict(result)
        else:
            raise BadRequest('new workentry conflicts with another workentry')

    def detail(self, pk):
        """Returns a single workentry, which id is equal pk.
        if no workentry's id match pk, the system returns http error 404.

        :param pk: workentry's id
        :return:{'id': 'pk', 'created_at': 'created date', 'start': 'start date',
        #            'finish': 'finish date', 'user': 'username', 'project': 'project name',
        #            'assignment_id': assignment id, 'obs': 'obs'}
        """
        try:
            workentry = (self._base_query.where(WorkEntry.id == pk)
                         .aggregate_rows().get())
        except WorkEntry.DoesNotExist:
            raise NotFound()

        return model_to_dict(workentry)

WorkEntryResource.add_url_rules(mod_api, rule_prefix='/workentries/')


class WorkEntryManagementResource(FreeShiftResource):
    _base_query = _base_query = (WorkEntry.select()
                                 .join(Assignment, JOIN.LEFT_OUTER)
                                 .join(Project, JOIN.LEFT_OUTER)
                                 .join(Contract, JOIN.LEFT_OUTER,
                                       on=Assignment.contract == Contract.id))

    def update(self, pk):
        try:
            work_entry = (self._base_query.where(WorkEntry.id == pk)
                         .aggregate_rows().get())
        except WorkEntry.DoesNotExist:
            raise NotFound()

        # Validate if user has access to the project
        if not self.user_has_access_assignment(work_entry.assignment.project.id):
            raise Unauthorized()

        # Validate if entry is already approved/reproved
        if not work_entry.status == 'WAITING_APPROVAL':
            raise BadRequest('Entry already approved')

        work_entry.status = ('APPROVED' if (self.data['approved'])
                             else 'REPROVED')
        work_entry.save()
        return model_to_dict(work_entry)

mod_api.add_url_rule('/workentry/<pk>/manage/',
                     endpoint='api_workday_management_update',
                     view_func=WorkEntryManagementResource.as_detail(),
                     methods=['PUT'])


class ProjectResource(FreeShiftResource):
    def create(self, *args, **kwargs):
        self.validate_data(['name', 'company_id', 'color'])
        # Validate if user is company admin|coordinator
        if not self.user_has_access_company(self.data['company_id']):
            raise Unauthorized()
        # Validate if project with name already exists
        if Project.select().where(Project.name == self.data['name'],
                                  Project.company == self.data['company_id']).exists():
            raise BadRequest('Project with this name already exists for this company')
        # Persist
        new_project = Project.create(name=self.data['name'],
                                     company=self.data['company_id'],
                                     color=self.data['color'])
        # Assign user to project
        contract = (Contract.select()
                    .join(Calendar, JOIN.LEFT_OUTER)
                    .join(Company, JOIN.LEFT_OUTER)
                    .where(Company.id == new_project.company,
                           Contract.user == self.user.id)
                    .get())

        Assignment.create(contract=contract.id, project=new_project.id,
                          project_role='COORDINATOR', color=self.data['color'])
        return model_to_dict(new_project)

    @FreeShiftResource.access_validation(FreeShiftResource.user_has_access_assignment)
    def detail(self, pk):
        try:
            result = Project.get(Project.id == pk)
        except Project.DoesNotExist:
            raise NotFound()
        return model_to_dict(result)

    @FreeShiftResource.access_validation(FreeShiftResource.user_has_access_assignment)
    def delete(self, pk):
        try:
            result = Project.get(Project.id == pk)
        except Project.DoesNotExist:
            raise NotFound()
        result.delete_instance()

    @FreeShiftResource.access_validation(FreeShiftResource.user_has_access_assignment)
    def update(self, pk):
        try:
            result = Project.get(Project.id == pk)
        except Project.DoesNotExist:
            raise NotFound()

        if self.data['name'] is not None:
            result.name=self.data['name']

        if self.data['color'] is not None:
            result.color=self.data['color']

        return model_to_dict(result.save())

ProjectResource.add_url_rules(mod_api, rule_prefix='/projects/')


class ProjectWorkDayResource(FreeShiftResource):
    _base_query = (WorkEntry.select(WorkEntry.id,
                                    WorkEntry.created_at,
                                    WorkEntry.start, WorkEntry.finish,
                                    User.name.alias('user'),
                                    Project.name.alias('project'),
                                    Project.color.alias('project_color'),
                                    Assignment.id.alias('assignment_id'),
                                    WorkEntry.obs,
                                    WorkEntry.status)
                   .join(Assignment, JOIN.LEFT_OUTER)
                   .join(Project, JOIN.LEFT_OUTER)
                   .join(Contract, JOIN.LEFT_OUTER,
                         on=Assignment.contract == Contract.id)
                   .join(User, JOIN.LEFT_OUTER))

    @FreeShiftResource.access_validation(FreeShiftResource.user_has_access_assignment)
    def list(self, pk, *args, **kwargs):
        self.get_user_from_token()
        start_date = request.args.get('start_date')
        end_date = request.args.get('end_date')

        if end_date is None:
            raise BadRequest()

        if start_date is None:
            raise BadRequest()

        query = (self._base_query.where(Project.id == pk,
                                        WorkEntry.start >= start_date,
                                        WorkEntry.start <= end_date))

        user_id = request.args.get('user_id')
        if user_id is not None:
            query = query.where(User.id == user_id)

        if request.args.get('waiting_approval') == 'true':
            query = query.where(WorkEntry.status == 'WAITING_APPROVAL')

        result = (query
                  .order_by(WorkEntry.start.asc(), User.name.asc())
                  .aggregate_rows().dicts())

        return_value = []
        last_date = None
        for item in result:
            if item['start'].date() != last_date:
                last_date = item['start'].date()
                return_value.append({'date': last_date, 'work_entries': []})

            return_value[-1]['work_entries'].append(item)

        return return_value

mod_api.add_url_rule('/project/<pk>/workdays/',
                     endpoint='api_project_workdays_list',
                     view_func=ProjectWorkDayResource.as_list(),
                     methods=['GET'])


class ProjectUsersResource(FreeShiftResource):
    @FreeShiftResource.access_validation(FreeShiftResource.user_has_access_assignment)
    def list(self, pk):
        result = (User.select(User.id, User.name)
                  .join(Contract, JOIN.LEFT_OUTER)
                  .join(Calendar, JOIN.LEFT_OUTER)
                  .join(Company, JOIN.LEFT_OUTER)
                  .join(Assignment, JOIN.LEFT_OUTER, on=(Assignment.contract == Contract.id))
                  .join(Project, JOIN.LEFT_OUTER)
                  .where(Project.id == pk)
                  .aggregate_rows().dicts())
        return result

mod_api.add_url_rule('/project/<pk>/users/',
                     endpoint='api_project_users_list',
                     view_func=ProjectUsersResource.as_list(),
                     methods=['GET'])


class ProjectAssignmentsResource(FreeShiftResource):
    @FreeShiftResource.access_validation(FreeShiftResource.user_has_access_assignment)
    def create(self, pk):
        try:
            project = Project.get(Project.id == pk)
        except Project.DoesNotExist:
            raise BadRequest()

        self.validate_data(['assignments'])
        contracts = list()
        company_id = None
        for assignment in self.data['assignments']:
            contract_id = assignment['contract_id']
            try:
                contract = (Contract.select().join(Calendar)
                            .where(Contract.id == contract_id).get())
            except Contract.DoesNotExist:
                raise BadRequest('Contract id ' + str(contract_id) + ' does not exists')
            if company_id is None:
                company_id = contract.calendar.company.id
            elif company_id != contract.calendar.company.id:
                raise BadRequest('Contract id ' + str(contract_id) + ' is for a different company')
            contracts.append((contract, assignment['project_role']))

        # Validate if user is company admin|coordinator
        if not self.user_has_access_company(company_id):
            raise Unauthorized()

        color = project.color
        if self.data['color'] is not None:
            color = self.data['color']

        assignments = list()
        #check if user is already assigned to project
        for contract_role in contracts:
            if (Assignment.select()
                        .where(Assignment.contract == contract_role[0].id,
                               Assignment.project == pk)).exists():
                raise BadRequest('User ' + contract_role[0].user.name + ' already assigned to this project')

            assignments.append(Assignment.create(contract=contract_role[0].id,
                                                 project=pk,
                                                 project_role=contract_role[1], color=color))

        return model_to_dict(assignments)

mod_api.add_url_rule('/project/<pk>/assignments/',
                     endpoint='api_project_assignments_list',
                     view_func=ProjectAssignmentsResource.as_list(),
                     methods=['POST'])


class CompanyResource(FreeShiftResource):
    def list(self):
        # TODO WRITE DOCSTRING DESCRIBING THE RETURNED DATA
        companies = Company.select()
        return [model_to_dict(company) for company in companies]

    def create(self):
        self.validate_data(['name'])
        if Company.select().where(Company.name == self.data['name']).exists():
            raise BadRequest('Company with this name already exists')
        company = Company()
        company.name = self.data['name']
        company.save()
        return model_to_dict(company)


CompanyResource.add_url_rules(mod_api, rule_prefix='/companies/')


class CompanyProjectsResource(FreeShiftResource):
    @FreeShiftResource.access_validation(FreeShiftResource.user_has_access_company)
    def list(self, pk):
        projects = (Project.select(Project.id, Project.name)
                    .join(Company)
                    .where(Company.id == pk)
                    .aggregate_rows().dicts())
        return projects

mod_api.add_url_rule('/company/<pk>/projects/',
                     endpoint='api_company_projects_list',
                     view_func=CompanyProjectsResource.as_list(),
                     methods=['GET'])


class CompanyUsersResource(FreeShiftResource):
    @FreeShiftResource.access_validation(FreeShiftResource.user_has_access_company)
    def list(self, pk):
        users = (User
                 .select(User.name, User.id)
                 .join(Contract, JOIN.LEFT_OUTER)
                 .join(Calendar, JOIN.LEFT_OUTER)
                 .join(Company, JOIN.LEFT_OUTER)
                 .where(Company.id == pk)
                 .aggregate_rows().dicts())
        return users

mod_api.add_url_rule('/company/<pk>/users/',
                     endpoint='api_company_users_list',
                     view_func=CompanyUsersResource.as_list(),
                     methods=['GET'])


class CompanyWorkdaysResource(FreeShiftResource):
    _base_query = (WorkEntry.select(WorkEntry.id,
                                    WorkEntry.created_at,
                                    WorkEntry.start, WorkEntry.finish,
                                    User.name.alias('user'),
                                    Project.name.alias('project'),
                                    Project.color.alias('project_color'),
                                    Assignment.id.alias('assignment_id'),
                                    WorkEntry.obs,
                                    WorkEntry.status)
                   .join(Assignment, JOIN.LEFT_OUTER)
                   .join(Project, JOIN.LEFT_OUTER)
                   .join(Contract, JOIN.LEFT_OUTER,
                         on=Assignment.contract == Contract.id)
                   .join(Company, JOIN.LEFT_OUTER, on=Project.company == Company.id)
                   .join(User, JOIN.LEFT_OUTER, on=Contract.user == User.id))

    @FreeShiftResource.access_validation(FreeShiftResource.user_has_access_company)
    def list(self, pk):
        self.get_user_from_token()
        start_date = request.args.get('start_date')
        end_date = request.args.get('end_date')

        if end_date is None:
            raise BadRequest()

        if start_date is None:
            raise BadRequest()

        query = (self._base_query.where(Company.id == pk,
                                        WorkEntry.start >= start_date,
                                        WorkEntry.start <= end_date))

        user_id = request.args.get('user_id')
        if user_id is not None:
            query = query.where(User.id == user_id)

        if request.args.get('waiting_approval') == 'true':
            query = query.where(WorkEntry.status == 'WAITING_APPROVAL')

        project_id = request.args.get('project_id')
        if project_id is not None:
            query = query.where(Project.id == project_id)

        result = (query
                  .order_by(WorkEntry.start.asc(), Project.id.desc(), User.name.asc())
                  .aggregate_rows().dicts())

        return_value = []
        last_date = None
        for item in result:
            if item['start'].date() != last_date:
                last_date = item['start'].date()
                return_value.append({'date': last_date, 'work_entries': []})

            return_value[-1]['work_entries'].append(item)

        return return_value

mod_api.add_url_rule('/company/<pk>/workdays/',
                     endpoint='api_company_workdays_list',
                     view_func=CompanyWorkdaysResource.as_list(),
                     methods=['GET'])


class CompanyCalendarsResource(FreeShiftResource):
    @FreeShiftResource.access_validation(FreeShiftResource.user_has_access_company)
    def list(self, pk):
        return Calendar.select().where(Calendar.company == pk).dicts()

mod_api.add_url_rule('/company/<pk>/calendars/',
                     endpoint='api_company_contracts_list',
                     view_func=CompanyCalendarsResource.as_list(),
                     methods=['GET'])