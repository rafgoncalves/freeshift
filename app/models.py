from config import DATABASE
from peewee import Model, CharField, DateField, IntegerField, \
    ForeignKeyField
from playhouse.postgres_ext import DateTimeTZField


CONTRACT_TYPE = [('COORDINATOR', 'COORDINATOR'),
                 ('WORKER', 'WORKER'),
                 ('ADMIN', 'ADMIN')]

PROJECT_ROLE = [('COORDINATOR', 'COORDINATOR'),
                ('WORKER', 'WORKER')]

WORK_ENTRY_STATUS = [('WAITING_APPROVAL', 'WAITING_APPROVAL'),
                     ('APPROVED', 'APPROVED'),
                     ('REPROVED', 'REPROVED')]

db = DATABASE


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    name = CharField(max_length=256, null=False)
    login = CharField(max_length=30, unique=True, null=False)
    password = CharField(max_length=256, null=False)
    email = CharField(max_length=256, unique=True, null=False)


class Company(BaseModel):
    name = CharField(max_length=256)


class Project(BaseModel):
    company = ForeignKeyField(Company, related_name='projects')
    name = CharField(max_length=256)
    color = CharField(max_length=7)


class Calendar(BaseModel):
    company = ForeignKeyField(Company, related_name='calendars')
    name = CharField(max_length=256)
    obs = CharField(max_length=1000, null=True)
    default_monday = IntegerField()
    default_tuesday = IntegerField()
    default_wednesday = IntegerField()
    default_thursday = IntegerField()
    default_friday = IntegerField()
    default_saturday = IntegerField()
    default_sunday = IntegerField()


class CustomDay(BaseModel):
    calendar = ForeignKeyField(Calendar, related_name='custom_days')
    date = DateField()
    expectedHours = IntegerField()


class Contract(BaseModel):
    calendar = ForeignKeyField(Calendar)
    user = ForeignKeyField(User, related_name='contracts')
    contract_type = CharField(choices=CONTRACT_TYPE)


class Assignment(BaseModel):
    contract = ForeignKeyField(Contract, related_name='assignments')
    project = ForeignKeyField(Project)
    project_role = CharField(choices=PROJECT_ROLE)
    color = CharField(max_length=7)


class WorkEntry(BaseModel):
    assignment = ForeignKeyField(Assignment, related_name='work_entries')
    created_at = DateTimeTZField()
    start = DateTimeTZField()
    finish = DateTimeTZField()
    obs = CharField(max_length=1000, null=True)
    status = CharField(choices=WORK_ENTRY_STATUS, default='WAITING_APPROVAL')
