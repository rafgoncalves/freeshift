from app import app
import sys


class Manage():
    @staticmethod
    def setup_db():
        from app.models import User, Company, Project, Calendar, Assignment, \
            CustomDay, Contract, WorkEntry, DATABASE

        DATABASE.create_tables([User, Company, Project, Calendar, Assignment,
                               CustomDay, Contract, WorkEntry])

    @staticmethod
    def server():
        app.run(host = '0.0.0.0')

if len(sys.argv) > 1:
    option = sys.argv[1]
    command = getattr(Manage, option)

    if command is None:
        exit('[ERROR] %s is not a command' % option)

else:
    command = Manage.server

command()
