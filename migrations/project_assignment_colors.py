__author__ = 'gabriel.pinheiro'

from config import DATABASE
from playhouse.migrate import PostgresqlMigrator, CharField, IntegerField, migrate

migrator = PostgresqlMigrator(DATABASE)

color = CharField(max_length=7, default='#000000')

with DATABASE.transaction():
    migrate(
        migrator.add_column('project', 'color', color),
        migrator.add_column('assignment', 'color', color),
    )
