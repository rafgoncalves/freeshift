from datetime import date
from peewee import JOIN
from app.models import User, Company, Project, Calendar, CustomDay, Contract, Assignment

names = ['Alice', 'Bob', 'Charlie', 'Dana', 'Frank', 'Gene']
companies = ['AT&T', 'BOSCH', 'CATERPILLAR']
contracts = {'Alice': 'AT&T', 'Bob': 'AT&T', 'Charlie': 'BOSCH', 'Dana': 'BOSCH', 'Frank': 'CATERPILLAR',
             'Gene': 'CATERPILLAR'}
contract_types = {'Alice': 'ADMIN', 'Bob': 'WORKER', 'Charlie': 'ADMIN', 'Dana': 'WORKER', 'Frank': 'ADMIN',
                  'Gene': 'COORDINATOR'}
user_calendar = {'Alice': 'CLT', 'Bob': 'CLT', 'Charlie': 'PJ', 'Dana': 'Intern', 'Frank': 'CLT', 'Gene': 'CLT'}
calendars = ['CLT', 'PJ', 'Intern']
projects = ['Square', 'Circle', 'Diamond']
roles = {'Alice': 'COORDINATOR', 'Bob': 'WORKER', 'Charlie': 'COORDINATOR', 'Dana': 'WORKER', 'Frank': 'COORDINATOR',
         'Gene': 'COORDINATOR'}

for name in names:
    User.create(name=name,
                login=name.lower(),
                password=name,
                email=('%s@abc.com' % name))

for company in companies:
    c = Company.create(name=company)

    for project in projects:
        name = '/'.join([project, company])
        Project.create(company=c, name=name)

    for calendar in calendars:
        cal = Calendar.create(company=c,
                              name=calendar,
                              obs=company,
                              default_monday=8,
                              default_tuesday=8,
                              default_wednesday=8,
                              default_thursday=8,
                              default_friday=8,
                              default_saturday=8,
                              default_sunday=8,
                              )

        cd = CustomDay.create(calendar=cal,
                              date=date(year=2015, month=5, day=10),
                              expectedHours=2
                              )

for name in names:
    u = User.get(User.login == name.lower())
    comp = Company.get(Company.name==contracts.get(name))
    cal = Calendar.get(Calendar.name == user_calendar.get(name) and Calendar.company == comp.id )

    c_type = contract_types.get(name)
    # create contract
    cont = Contract.create(calendar=cal, user=u, contract_type=c_type)
    for p in Project.select().where(Project.company == cal.company.id):
        Assignment.create(contract=cont, project=p, project_role=roles.get(name))



    # class CustomDay(BaseModel):
    # calendar = ForeignKeyField(Calendar, related_name='custom_days')
    #     date = DateField()
    #     expectedHours = IntegerField()